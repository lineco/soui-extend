#include "stdafx.h"
#include "SBarContrastChart.h"

SCoordAxis2::SCoordAxis2()
{
	m_TextOffset = 8;
	m_OriginValue = 0;
}

SCoordAxis2::~SCoordAxis2()
{


}

void SCoordAxis2::OnPaint(IRenderTarget *pRT)
{
	//绘制轴线
	SPainter painter;
	BeforePaint(pRT, painter);

	CAutoRefPtr<IRenderObj> curPen = pRT->GetCurrentObject(OT_PEN);

	//计算矩形
	CRect rcClient = GetClientRect();
	CRect rectDraw = rcClient;

	//绘制坐标轴
	CRect Margin = m_CoordMargin;
	POINT CoordX[2];
	POINT CoordY[2];
	CoordY[0].x = rectDraw.left + Margin.left;
	CoordY[0].y = rectDraw.top + Margin.top;
	CoordY[1].x = CoordY[0].x;
	CoordY[1].y = rectDraw.bottom - Margin.bottom;

	CoordX[0].x = CoordY[0].x;
	CoordX[0].y = CoordY[0].y + (CoordY[1].y - CoordY[0].y) / 2;
	CoordX[1].x = rectDraw.right - Margin.right;
	CoordX[1].y = CoordX[0].y;

	m_DataRect.left = CoordX[0].x + m_CoordWidth;
	m_DataRect.top = CoordY[0].y;
	m_DataRect.right = CoordX[1].x - m_CoordWidth;
	m_DataRect.bottom = CoordY[1].y;

	CAutoRefPtr<SOUI::IPen> Pen;
	pRT->CreatePen(PS_SOLID, m_CoordColor, m_CoordWidth, &Pen);
	pRT->SelectObject(Pen);
	pRT->DrawLines(CoordX,2);
	pRT->DrawLines(CoordY, 2);

	int Count = m_Text.GetCount();

	//获取数值的最小值和最大值
	m_MinValue = m_MaxValue = 0;
	GetMaxMin(m_MaxValue, m_MinValue);

	m_MinValue = m_OriginValue;
	m_ValueSpace = (m_MaxValue - m_MinValue) / (float)(m_ScaleNumber)/2;//标线间距数值

	m_XSignRatio = 0;//X方向标线比例
	m_XCount = 0;
	m_YSignRatio = 0;//Y方向标线比例
	m_YCount = 0;
	m_ValueRatio = 0;//数值比例

	m_XCount = Count + 1;
	m_YCount = m_ScaleNumber*2;
	m_XSignRatio = m_DataRect.Width() / (float)m_XCount;
	m_YSignRatio = m_DataRect.Height() / (float)m_YCount;
	m_ValueRatio =(m_DataRect.Height()/2) / (m_MaxValue - m_MinValue);
	
	
	Pen.Release();
	pRT->CreatePen(PS_SOLID, m_ScaleLineColor, m_ScaleLineWidth, &Pen);
	pRT->SelectObject(Pen);

	//绘制X轴上标线与文字
	int n = 0;
	for (int i = 0; i <= m_XCount&&m_XCount>0 && m_XScaleLength>0; i++)
	{
		POINT Coord[2][2] = { 0 };
		Coord[0][0].x = m_DataRect.left + m_XSignRatio*i;
		Coord[0][0].y = m_DataRect.CenterPoint().y - m_ScaleLineWidth;
		Coord[0][1].x = Coord[0][0].x;
		Coord[0][1].y = Coord[0][0].y - (m_XScaleLength > 1.0 ? m_XScaleLength : m_XScaleLength*(m_DataRect.Height()/2));

		Coord[1][0].x = Coord[0][0].x;
		Coord[1][0].y = Coord[0][0].y + m_ScaleLineWidth*2;
		Coord[1][1].x = Coord[0][1].x;
		Coord[1][1].y = Coord[1][0].y + (m_XScaleLength > 1.0 ? m_XScaleLength : m_XScaleLength*(m_DataRect.Height()/2));

		 if (i > 0)
		{
			pRT->DrawLines(Coord[0],2);
			pRT->DrawLines(Coord[1],2);
		}

		SIZE size;
		if (i>0&&n<Count)
		{
			SStringT *pStr = &m_Text[n++];
			pRT->MeasureText(*pStr, pStr->GetLength(), &size);
			pRT->TextOut(Coord[1][1].x - size.cx / 2, Coord[1][1].y + m_TextOffset, *pStr, -1);
		}
	}

	//绘制Y轴上标线与文字
	n = 0;
	for (int i = 0; i <= m_YCount&&m_YCount>0 && m_YScaleLength>0; i++)
	{
		POINT Coord[2] = { 0 };
		Coord[0].x = m_DataRect.left;
		Coord[0].y = m_DataRect.bottom - m_YSignRatio*i;
		Coord[1].x = Coord[0].x + (m_YScaleLength > 1.0 ? m_YScaleLength : m_YScaleLength*m_DataRect.Width());
		Coord[1].y = Coord[0].y;

		if (i >= 0 && i != m_YCount/2)
			pRT->DrawLines(Coord, 2);

		SIZE size;
		 if ((m_ShowOrigin || i>0))
		{
			SStringT Str;
			NumberToScaleStr(m_MinValue + m_ValueSpace*(i), m_Decimal, Str);
			pRT->MeasureText(Str, Str.GetLength(), &size);
			pRT->TextOut(Coord[0].x - size.cx - m_TextOffset, Coord[0].y - size.cy / 2, Str, -1);
		}
	}


	pRT->SelectObject(curPen);
	AfterPaint(pRT, painter);
}





SBarContrastChart::SBarContrastChart():
m_BarWidth(0.2),
m_BarColor(RGBA(255,0,0,255)),
m_BarInterval(0.4),
m_ShowValue(true),
m_Unit(_T("")),
m_BarTextColor(RGBA(0,0,0,0))
{
	m_PullOver = false;

}


SBarContrastChart::~SBarContrastChart()
{

}

void SBarContrastChart::AddData(int id, float Value)
{	
	if (m_Data[id].GetCount() > 0)
		AddData(id, Value, m_Data[id][m_Data[id].GetCount() - 1].Color);
	else
		AddData(id, Value, m_BarColor);
}

void SBarContrastChart::AddData(int id, float Value, COLORREF Color)
{
	BarDataType t;
	t.Value = Value;
	t.Color = Color;
	m_Data[id].Add(t);
}



void SBarContrastChart::Clear(void)
{
	m_Data.RemoveAll();
}

bool SBarContrastChart::Remove(int id)
{
	return m_Data.RemoveKey(id);
}

void SBarContrastChart::OnPaint(IRenderTarget *pRT)
{
	__super::OnPaint(pRT);
	SPainter painter;
	BeforePaint(pRT, painter);

	CAutoRefPtr<IRenderObj> curBrush = pRT->GetCurrentObject(OT_BRUSH);
	CAutoRefPtr<IRenderObj> curPen = pRT->GetCurrentObject(OT_PEN);
	CAutoRefPtr<IRenderObj> curFont = pRT->GetCurrentObject(OT_FONT);
	COLORREF curTextColor=pRT->GetTextColor();

	SOUI::SPOSITION pos = m_Data.GetStartPosition();
	float BarMaxWidth;
	int BarTypeCount = m_Data.GetCount();
	int BarWidth = 0;
	int BarInterval = 0;
	int index=0;
	while (pos&&!m_PullOver)
	{
		CoordDataType::CPair *p = m_Data.GetNext(pos);
		for (size_t i = 0; i <p->m_value.GetCount(); i++)
		{
			CRect Bar;

			if (m_AxisType == AXIS_LENGTHWAYS)
			{
				BarInterval = m_BarInterval <= 1 ? m_XSignRatio / 2 * m_BarInterval : m_BarInterval;
				BarMaxWidth = m_XSignRatio-BarInterval;

				float SingleMaxWidth = (BarMaxWidth / (float)BarTypeCount);
				BarWidth = m_BarWidth <= 1 ? SingleMaxWidth*m_BarWidth : m_BarWidth;
				if (BarWidth >SingleMaxWidth)
					BarWidth = SingleMaxWidth;
				float SingleOffset = m_XSignRatio*(i)+m_XSignRatio / 2 + ((SingleMaxWidth*index) + SingleMaxWidth / 2) + BarInterval/2;//单个柱中心位置

				Bar.left = GetDataDrawRect().left + SingleOffset - BarWidth/2;
				Bar.right = Bar.left + BarWidth;
				Bar.top = GetDataDrawRect().bottom - (m_ValueRatio*(p->m_value[i].Value - m_MinValue));
				Bar.bottom = GetDataDrawRect().bottom;

				pRT->FillSolidRect(Bar, p->m_value[i].Color);

				if (m_ShowValue)
				{
					SStringT str;
					SIZE size;
					NumberToScaleStr(p->m_value[i].Value, m_Decimal,str);
					str += m_Unit;
					if (m_BarTextFont.GetFontPtr() != NULL)	pRT->SelectObject(m_BarTextFont.GetFontPtr());
					if (m_BarTextColor!=RGBA(0,0,0,0)) pRT->SetTextColor(m_BarTextColor);
					pRT->MeasureText(str, str.GetLength(), &size);
					pRT->TextOut(Bar.CenterPoint().x - size.cx/2,Bar.top - size.cy, str, -1);
				}
			}

		}
		index++;
	}
	pRT->SelectObject(curBrush);
	pRT->SelectObject(curPen);
	pRT->SelectObject(curFont);
	pRT->SetTextColor(curTextColor);

	AfterPaint(pRT, painter);
}



void SBarContrastChart::GetMaxMin(float &Max, float &Min)
{
	SOUI::SPOSITION pos = m_Data.GetStartPosition();
	bool flag = false;
	while (pos)
	{
		CoordDataType::CPair *p = m_Data.GetNext(pos);
		for (size_t i = 0; i<p->m_value.GetCount(); i++)
		{
			float TempValue = p->m_value[i].Value;
			if (i == 0 && !flag)
			{
				Max = Min = TempValue;
				flag = true;
			}

			if (Min > TempValue)
				Min = TempValue;
			if (Max < TempValue)
				Max = TempValue;
		}
	}
}

void SBarContrastChart::SetUnit(SStringT Unit)
{
	m_Unit = Unit;
}

void SBarContrastChart::ShowValue(bool Show)
{
	m_ShowValue = Show;
}

void SBarContrastChart::SetBarInterval(float Interval)
{
	m_BarInterval = Interval;
}

void SBarContrastChart::SetBarWidth(float Width)
{
	m_BarWidth = Width;
}

void SBarContrastChart::SetData(int id, SArray<BarDataType> &Data)
{
	m_Data[id] = Data;
}

SArray<SBarContrastChart::BarDataType> &SBarContrastChart::GetData(int id)
{
	return m_Data[id];
}