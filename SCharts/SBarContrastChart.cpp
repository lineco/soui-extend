#include "stdafx.h"
#include "SBarContrastChart.h"
#include <GdiPlus.h>
#pragma comment(lib,"gdiplus")
namespace SOUI
{
	SBarContrastChart::SBarContrastChart() :
		m_CoordMargin(50, 50, 50, 50),
		m_BaseLineColor(RGBA(22, 220, 208, 255)),
		m_BaseLineWidth(2),
		m_BarWidth(0.2),
		m_BarColor(RGBA(255, 0, 0, 255)),
		m_ShowValue(true),
		m_BarTextColor(RGBA(0, 0, 0, 255))
	{

	}


	SBarContrastChart::~SBarContrastChart()
	{

	}

	void SBarContrastChart::AddBar(BarAttribute &Bar)
	{
		m_Data.push_back(Bar);
	}


	void SBarContrastChart::Clear(void)
	{
		m_Data.clear();
	}

	bool SBarContrastChart::Remove(int index)
	{
		/*return m_Data.RemoveKey(id);*/
		return false;
	}

	void SBarContrastChart::OnPaint(IRenderTarget *pRT)
	{
		SPainter painter;
		BeforePaint(pRT, painter);
		CAutoRefPtr<IRenderObj> curBrush = pRT->GetCurrentObject(OT_BRUSH);
		CAutoRefPtr<IRenderObj> curPen = pRT->GetCurrentObject(OT_PEN);
		CAutoRefPtr<IRenderObj> curFont = pRT->GetCurrentObject(OT_FONT);
		COLORREF curTextColor = pRT->GetTextColor();
		HDC hDC = pRT->GetDC();
		Gdiplus::Graphics graph(hDC);
		graph.SetSmoothingMode(Gdiplus::SmoothingModeHighQuality);

		//计算矩形
		CRect rcClient = GetClientRect();
		CRect rectDraw = rcClient;

		//绘制坐标轴
		CRect Margin = m_CoordMargin;

		Gdiplus::PointF Pof[2];
		Pof[0].X = rectDraw.left + Margin.left;
		Pof[0].Y = (rectDraw.top + Margin.top) + ((rectDraw.bottom - Margin.bottom) - (rectDraw.top + Margin.top)) / 2;
		Pof[1].X = rectDraw.right - Margin.right;
		Pof[1].Y = Pof[0].Y;
		graph.DrawLines(&Gdiplus::Pen(Gdiplus::Color(GetAValue(m_BaseLineColor), GetRValue(m_BaseLineColor), GetGValue(m_BaseLineColor), GetBValue(m_BaseLineColor)), m_BaseLineWidth), Pof, 2);

		m_DataRect.left = Pof[0].X;
		m_DataRect.top = rectDraw.top + Margin.top;
		m_DataRect.right = Pof[1].X;
		m_DataRect.bottom = rectDraw.bottom - Margin.bottom;

		float XSignRatio;
		int XCount = m_Data.size();
		XSignRatio = m_DataRect.Width() / (float)XCount;

		int i = 0;
		SIZE size;
		//	pRT->DrawRectangle(m_DataRect);
		for (BarAttribute &Bar : m_Data)
		{
			//绘制标题
			pRT->MeasureText(Bar.Ttitle, Bar.Ttitle.GetLength(), &size);
			pRT->TextOut(i*XSignRatio + m_DataRect.left - size.cx / 2 + XSignRatio / 2, m_DataRect.bottom + size.cy + 2, Bar.Ttitle, -1);

			//绘制矩形
			float ValueRatio = m_DataRect.Height() / 2.f / (float)(Bar.MaxValue - Bar.MinValue);

			for (int m = 0; m < 2; m++)
			{
				Gdiplus::RectF BarRect;
				BarRect.Width = m_BarWidth <= 1 ? XSignRatio*m_BarWidth : m_BarWidth;
				BarRect.X = m_DataRect.left + XSignRatio*i + (XSignRatio - BarRect.Width) / 2.F;
				BarRect.Height = ValueRatio*Bar.Bar[m].Value;

				if (m == 0)
					BarRect.Y = m_DataRect.bottom - BarRect.Height - m_DataRect.Height() / 2.F;
				else
					BarRect.Y = m_DataRect.top + m_DataRect.Height() / 2.F;

				Gdiplus::RectF GradientBrushRect = BarRect;
				int h = Bar.Bar[m].GraduaIsMaxRate ? \
					(Bar.Bar[m].GradualHeight > 1 ? Bar.Bar[m].GradualHeight : Bar.Bar[m].GradualHeight*(m_DataRect.Height() / 2))\
					:(Bar.Bar[m].GradualHeight > 1 ? Bar.Bar[m].GradualHeight : Bar.Bar[m].GradualHeight*BarRect.Height);
				GradientBrushRect.Height = h > BarRect.Height ? BarRect.Height : h;
				GradientBrushRect.Y = (m == 0 ? Pof[m].Y - GradientBrushRect.Height : BarRect.Y + m_BaseLineWidth);
				if (m == 1) GradientBrushRect.Offset(Gdiplus::PointF(0, -1));

				Gdiplus::LinearGradientBrush  GradientBrush(GradientBrushRect,
					Gdiplus::Color(GetAValue(Bar.Bar[m].ColorBegin), GetRValue(Bar.Bar[m].ColorBegin), GetGValue(Bar.Bar[m].ColorBegin), GetBValue(Bar.Bar[m].ColorBegin)), \
					Gdiplus::Color(GetAValue(Bar.Bar[m].ColorEnd), GetRValue(Bar.Bar[m].ColorEnd), GetGValue(Bar.Bar[m].ColorEnd), GetBValue(Bar.Bar[m].ColorEnd)), \
					m == 0 ? 90 : 270);

				Gdiplus::RectF BrushRect = BarRect;
				Gdiplus::SolidBrush  Brush(Gdiplus::Color(GetAValue(Bar.Bar[m].ColorBegin), GetRValue(Bar.Bar[m].ColorBegin), GetGValue(Bar.Bar[m].ColorBegin), GetBValue(Bar.Bar[m].ColorBegin)));

				BrushRect.Height = BarRect.Height - GradientBrushRect.Height;
				BrushRect.Y = m == 0 ? BarRect.Y : BarRect.Y + GradientBrushRect.Height;

				graph.FillRectangle(&Brush, BrushRect);
				graph.FillRectangle(&GradientBrush, GradientBrushRect);

				if (m_ShowValue)
				{
					SOUI::IFontPtr poldFont = NULL;
					if (m_BarTextFont.GetFontPtr() != NULL)
						pRT->SelectObject(m_BarTextFont.GetFontPtr(), (IRenderObj**)&poldFont);
					COLORREF oldColor = pRT->SetTextColor(m_BarTextColor);
					//绘制文本
					SStringT str;
					str.Format(SStringT().Format(L"%%.%df %%s", Bar.Decimal), Bar.Bar[m].Value, Bar.Unit);
					pRT->MeasureText(str, str.GetLength(), &size);
					if (m == 0)
						pRT->TextOut(BarRect.X + (BarRect.Width / 2) - size.cx / 2, BarRect.GetTop() - size.cy, str, -1);
					else
						pRT->TextOut(BarRect.X + (BarRect.Width / 2) - size.cx / 2, BarRect.GetBottom() + 2, str, -1);

					if (poldFont != NULL) pRT->SelectObject(poldFont);
					pRT->SetTextColor(oldColor);
				}
			}
			i++;
		}

		pRT->ReleaseDC(hDC);
		pRT->SelectObject(curBrush);
		pRT->SelectObject(curPen);
		pRT->SelectObject(curFont);
		pRT->SetTextColor(curTextColor);
		AfterPaint(pRT, painter);
	}





	void SBarContrastChart::ShowValue(bool Show)
	{
		m_ShowValue = Show;
	}

	void SBarContrastChart::SetBarWidth(float Width)
	{
		m_BarWidth = Width;
	}


	void SBarContrastChart::SetV1Data(int index, float Value, bool IsUpdataView)
	{
		int i = 0;
		for (BarAttribute &Bar : m_Data)
		{
			if (i == index)
			{
				Bar.Bar[0].Value = Value;
			}
			i++;
		}
		if (IsUpdataView) UpdateWindow();
	}

	void SBarContrastChart::SetV2Data(int index, float Value, bool IsUpdataView)
	{
		int i = 0;
		for (BarAttribute &Bar : m_Data)
		{
			if (i == index)
			{
				Bar.Bar[1].Value = Value;
			}
			i++;
		}
		if (IsUpdataView) UpdateWindow();
	}

	void SBarContrastChart::SetData(int index, float Value1, float Value2, bool IsUpdataView)
	{
		int i = 0;
		for (BarAttribute &Bar : m_Data)
		{
			if (i == index)
			{
				Bar.Bar[0].Value = Value1;
				Bar.Bar[1].Value = Value2;
			}
			i++;
		}
		if (IsUpdataView) UpdateWindow();
	}
}