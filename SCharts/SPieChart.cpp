#include "StdAfx.h"
#include <math.h>
#include <list>
#include "SPieChart.h"
#include <GdiPlus.h>
#pragma comment(lib,"gdiplus")

namespace SOUI
{

	enum{
		TIP_MARGIN = 5,//留出一点边距好看一点
		TIP_OFFSET = 15//提示框相对于鼠标的偏移
	};
	SPieChart::SPieChart(void) :
		m_Sum(0),
		m_PieSize(0.8),
		m_ShowTip(true),
		m_TipLineLen(10),
		m_TipColor(RGBA(0, 0, 0, 0)),
		m_CursorTip(true),
		m_CursorTipColorBkgnd(RGBA(128, 128, 128, 128)),
		m_CursorEnlargeRadius(7),
		m_CursorTipTextColor(RGBA(0, 0, 0, 255)),
		m_Decimal(0),
		m_SeriesName(L""),
		m_Decrease(0),
		m_HollowSize(0),
		m_ShowHollowText(false),
		m_HollowTextColor(RGBA(0, 0, 0, 0)),
		m_CursorInPie(false),
		m_SmallCircle(0)
	{


	}

	SPieChart::~SPieChart(void)
	{
	}

	int SPieChart::AddPie(PIE &TagPie)
	{
		int id;
		if (m_VecPies.size() > 0)
			id = m_VecPies.back().id + 1;
		else
			id = 0;
		TagPie.id = id;
		m_VecPies.push_back(TagPie);
		return id;
	}

	void SPieChart::Clear()
	{
		m_VecPies.clear();
	}

	void SPieChart::SetPie(int id, PIE &TagPie)
	{
		for (size_t i = 0; i < m_VecPies.size(); i++)
		{
			if (m_VecPies[i].id == id)
			{
				m_VecPies[i] = TagPie;
				m_VecPies[i].id = id;
				break;
			}
		}
	}

	void SPieChart::RemoveSectorById(int Id)
	{
		std::vector<PIE>::iterator it = m_VecPies.begin();
		while (it != m_VecPies.end())
		{
			if (it->id == Id)
			{
				it = m_VecPies.erase(it);
				break;
			}
			else
				it++;
		}
	}

	PIE* SPieChart::GetSectorById(int Id)
	{

		for (size_t i = 0; i < m_VecPies.size(); i++)
		{
			if (m_VecPies[i].id == Id)
				return &m_VecPies[i];
		}
		return NULL;
	}

	void SPieChart::SetPieSize(float Width)
	{
		m_PieSize = Width;
	}

	void SPieChart::SetSectorMin(float Decrease)
	{
		m_Decrease = Decrease;
	}

	void SPieChart::SetHollow(float Size, bool ShowHollowText)
	{
		m_HollowSize = Size;
		m_ShowHollowText = ShowHollowText;
	}

	//文本的位置
	struct TipSite{
		Gdiplus::PointF Pos[3];
		Gdiplus::RectF Rect;
		TipSite(Gdiplus::PointF *Pos, Gdiplus::RectF &Rect)
		{
			this->Pos[0] = Pos[0];
			this->Pos[1] = Pos[1];
			this->Pos[2] = Pos[2];
			this->Rect = Rect;
		}
	};
	struct _Arg
	{
		float BeginArg;
		float EndArg;
		int x;//圆的中心x
		int y;//圆的中心y
		int MinR;//最小半径
		int MaxR;//最大半径
		float Wdith;//圆矩形宽度
		_Arg(float BeginArg, float EndArg, int x, int y, int MinR, int MaxR, float Wdith)
		{
			this->BeginArg = BeginArg;
			this->EndArg = EndArg;
			this->x = x;
			this->y = y;
			this->MinR = MinR;
			this->MaxR = MaxR;
			this->Wdith = Wdith;
		}
	};

	//简单排版位置
	void AdjustTextSite(int CenterX, int CenterY, std::vector<TipSite> &TipSites)
	{
		//矩形相交处理
		int count = TipSites.size();
		for (int i = 0; i<count; i++)
		{
			int last = (i - 1) < 0 ? count - 1 : i - 1;
			//右下角
			if (TipSites[i].Pos[1].X > CenterX&&TipSites[i].Pos[1].Y>CenterY)
			{
				if (TipSites[i].Rect.GetTop() < TipSites[last].Rect.GetBottom())
				{
					int k = TipSites[last].Rect.GetBottom() - TipSites[i].Rect.Y;
					TipSites[i].Rect.Y += abs(TipSites[last].Rect.GetBottom() - TipSites[i].Rect.Y);
					TipSites[i].Pos[2].Y = TipSites[i].Rect.Y + TipSites[i].Rect.Height / 2;
				}
			}//左上角
			else if (TipSites[i].Pos[1].X < CenterX&&TipSites[i].Pos[1].Y < CenterY)
			{
				if (TipSites[i].Rect.GetBottom() > TipSites[last].Rect.GetTop())
				{
					TipSites[i].Rect.Y -= abs(TipSites[i].Rect.GetBottom() - TipSites[last].Rect.Y);
					TipSites[i].Pos[2].Y = TipSites[i].Rect.Y + TipSites[i].Rect.Height / 2;
				}
			}
		}

		for (int i = count - 1; i > -1; i--)
		{
			int last = (i + 1) < count ? i + 1 : 0;
			//右上角
			if (TipSites[i].Pos[1].X > CenterX&&TipSites[i].Pos[1].Y < CenterY)
			{
				if (TipSites[i].Rect.GetBottom() > TipSites[last].Rect.GetTop())
				{
					TipSites[i].Rect.Y -= abs(TipSites[i].Rect.GetBottom() - TipSites[last].Rect.Y);
					TipSites[i].Pos[2].Y = TipSites[i].Rect.Y + TipSites[i].Rect.Height / 2;
				}
			}
			else//左下角
				if (TipSites[i].Pos[1].X<CenterX&&TipSites[i].Pos[1].Y>CenterY)
				{
					if (TipSites[i].Rect.GetTop() < TipSites[last].Rect.GetBottom())
					{
						TipSites[i].Rect.Y += abs(TipSites[last].Rect.GetBottom() - TipSites[i].Rect.GetTop());
						TipSites[i].Pos[2].Y = TipSites[i].Rect.Y + TipSites[i].Rect.Height / 2;
					}
				}

		}

	}


	void SPieChart::OnPaint(IRenderTarget *pRT)
	{
		SPainter painter;
		BeforePaint(pRT, painter);

		CAutoRefPtr<IRenderObj> curBrush = pRT->GetCurrentObject(OT_BRUSH);
		CAutoRefPtr<IRenderObj> curPen = pRT->GetCurrentObject(OT_PEN);
		CAutoRefPtr<IRenderObj> curFont = pRT->GetCurrentObject(OT_FONT);
		COLORREF curTextColor = pRT->GetTextColor();


		CRect rcClient = GetClientRect();

		Gdiplus::RectF rectDraw;
		rectDraw.X = rcClient.left;
		rectDraw.Y = rcClient.top;
		rectDraw.Width = rcClient.Width();
		rectDraw.Height = rcClient.Height();

		float f = rectDraw.Height = rectDraw.Width = rectDraw.Width > rectDraw.Height ? rectDraw.Height : rectDraw.Width;

		rectDraw.Height = rectDraw.Width = f*(m_PieSize < 1) ? m_PieSize*f : m_PieSize;

		rectDraw.X += (f - rectDraw.Width) / 2.f;
		rectDraw.Y += (f - rectDraw.Width) / 2.f;


		rectDraw.Offset(Gdiplus::PointF(rcClient.CenterPoint().x - (rectDraw.X + rectDraw.Width / 2), rcClient.CenterPoint().y - (rectDraw.Y + rectDraw.Height / 2)));

		float nSweep = 0;
		float r = rectDraw.Height / 2.f;
		float CenterX = rectDraw.X + rectDraw.Width / 2.f;
		float CenterY = rectDraw.Y + rectDraw.Width / 2.f;
		const double pi = acos(-1.0);
		float HollowWidth = m_HollowSize < 1 ? r*m_HollowSize * 2 : m_HollowSize;

		HDC hDC = pRT->GetDC();
		Gdiplus::Graphics graph(hDC);
		graph.SetSmoothingMode(Gdiplus::SmoothingModeHighQuality);
		//使用SOUI的字体
		CAutoRefPtr<SOUI::IFont> pIFont;
		pIFont = (SOUI::IFont*)pRT->GetCurrentObject(OT_FONT);
		Gdiplus::FontFamily fontfamily(pIFont->FamilyName());
		Gdiplus::Font font(&fontfamily, abs(pIFont->TextSize()), Gdiplus::FontStyleRegular, Gdiplus::UnitPixel);

		//GDI 计算绘制区域时使用Tip字体格式
		CAutoRefPtr<SOUI::IFont> pTipFont = m_TipFont.GetFontPtr();
		Gdiplus::Font TipFont(&fontfamily, abs(pTipFont ? pTipFont->TextSize() : pIFont->TextSize()), Gdiplus::FontStyleRegular, Gdiplus::UnitPixel);

		Gdiplus::PointF po;
		Gdiplus::RectF TxtRect;

		std::vector<_Arg> _Args;//角度
		std::vector<TipSite> TextRects;//存储了文字绘制的位置
		float  Decrease = r*m_Decrease / (float)m_VecPies.size();


		//获取m_Sum
		m_Sum = 0;
		for (size_t i = 0; i < m_VecPies.size(); i++)
		{
			m_Sum += m_VecPies[i].Value;
		}

		for (size_t i = 0; i < m_VecPies.size(); i++)
		{
			Gdiplus::Color color(GetAValue(m_VecPies[i].Color), GetRValue(m_VecPies[i].Color), GetGValue(m_VecPies[i].Color), GetBValue(m_VecPies[i].Color));
			Gdiplus::SolidBrush hBrush(color);
			hBrush.SetColor(color);
			float nTemp = m_VecPies[i].Value / m_Sum*360.f;
			float PieWidth = rectDraw.Width - Decrease*i * 2;

			if (m_HollowSize > 0)
			{
				float Width = (PieWidth - HollowWidth) / 2;
				Gdiplus::Pen ArcPen(color, Width);
				graph.DrawArc(&ArcPen, (rectDraw.X + Decrease*i) + Width / 2, (rectDraw.Y + Decrease*i) + Width / 2, PieWidth - Width, PieWidth - Width, nSweep, nTemp);

			}
			else
				graph.FillPie(&hBrush, rectDraw.X + Decrease*i, rectDraw.Y + Decrease*i, PieWidth, PieWidth, nSweep, nTemp);

			if (m_ShowTip)
			{
				Gdiplus::Pen curPen(color, 2);
				Gdiplus::PointF Pof[3];

				Pof[0].X = CenterX + PieWidth / 2 * cos(-(nSweep + abs(nTemp) / 2.f)* pi / 180.f);
				Pof[0].Y = CenterY - PieWidth / 2 * sin(-(nSweep + abs(nTemp) / 2.f)* pi / 180.f);

				Pof[1].X = CenterX + (r + m_TipLineLen)*cos(-(nSweep + abs(nTemp) / 2.f)* pi / 180.f);
				Pof[1].Y = CenterY - (r + m_TipLineLen)*sin(-(nSweep + abs(nTemp) / 2.f)* pi / 180.f);
				Pof[2].X = Pof[1].X > CenterX ? Pof[1].X + m_TipLineLen : Pof[1].X - m_TipLineLen;
				Pof[2].Y = Pof[1].Y;

				po = Pof[2];
				graph.MeasureString(m_VecPies[i].StrTip, m_VecPies[i].StrTip.GetLength(), &TipFont, po, &TxtRect);
				if (po.X < CenterX)
					po.X -= TxtRect.Width;
				po.Y -= TxtRect.Height / 2;
				TxtRect.X = po.X;
				TxtRect.Y = po.Y;

				TextRects.push_back(TipSite(Pof, TxtRect));
			}
			if (m_CursorTip)
			{
				_Args.push_back(_Arg(nSweep, nSweep + nTemp, rectDraw.X + Decrease*i, rectDraw.Y + Decrease*i, 0, (rectDraw.Width - Decrease*i * 2) / 2, rectDraw.Width - Decrease*i * 2));
			}
			nSweep += nTemp;
		}

		//调整布局
		AdjustTextSite(CenterX, CenterY, TextRects);
		//绘制文字与线
		for (size_t i = 0; i < TextRects.size(); i++)
		{
			TipSite &t = TextRects[i];
			COLORREF LineTextColor = m_TipColor != RGBA(0, 0, 0, 0) ? m_TipColor : m_VecPies[i].Color;

			Gdiplus::Color color(GetAValue(LineTextColor), GetRValue(LineTextColor), GetGValue(LineTextColor), GetBValue(LineTextColor));
			Gdiplus::SolidBrush hBrush(color);

			graph.DrawLines(&Gdiplus::Pen(color, 2), t.Pos, 3);

			CRect rect;
			rect.top = t.Rect.GetTop();
			rect.left = t.Rect.GetLeft();
			rect.bottom = t.Rect.GetBottom();
			rect.right = t.Rect.GetRight();

			pRT->SetTextColor(LineTextColor);
			if (m_TipFont.GetFontPtr() != NULL) pRT->SelectObject(m_TipFont.GetFontPtr());
			pRT->DrawText(m_VecPies[i].StrTip, m_VecPies[i].StrTip.GetLength(), rect, DT_LEFT | DT_VCENTER | DT_SINGLELINE);
		}

		if (m_HollowSize > 0)
		{
			//		Gdiplus::SolidBrush hBrush(Gdiplus::Color(GetAValue(GetStyle().m_crBg), GetRValue(GetStyle().m_crBg), GetGValue(GetStyle().m_crBg), GetBValue(GetStyle().m_crBg)));
			//		graph.FillEllipse(&hBrush, CenterX - HollowWidth / 2, CenterY - HollowWidth / 2, HollowWidth, HollowWidth);
			//小圆的绘制
			if (m_SmallCircle > 0 && (!m_CursorTip))
			{
				nSweep = 0;
				int LastBeginAngle = 0;//上一个圆的起始角度
				float SmallCircleAngle = asin((m_SmallCircle / 2.f) / r)*(180 / pi) * 2;;
				for (size_t i = 0; i < m_VecPies.size(); i++)
				{
					if (m_VecPies[i].Value == 0)
						continue;
					Gdiplus::RectF CircleRect;
					float nTemp = m_VecPies[i].Value / m_Sum*360.f;
					float PieWidth = rectDraw.Width - Decrease*i * 2;
					int x = CenterX + ((PieWidth - (PieWidth - HollowWidth) / 2) / 2) * cos(-(nSweep + abs(nTemp)/* / 2.f*/)* pi / 180.f);
					int y = CenterY - ((PieWidth - (PieWidth - HollowWidth) / 2) / 2) * sin(-(nSweep + abs(nTemp)/* / 2.f*/)* pi / 180.f);

					CircleRect.X = x - m_SmallCircle / 2;
					CircleRect.Y = y - m_SmallCircle / 2;
					CircleRect.Height = CircleRect.Width = m_SmallCircle;

					//防止圆圈被遮挡简单处理
					Gdiplus::Point po;
					po.X = CircleRect.X - CircleRect.Width / 2;
					po.Y = CircleRect.Y - CircleRect.Height / 2;
					float a = atan((po.Y - CenterY) / (po.X - CenterX))*(180 / pi);//弧度
					if (po.Y > CenterY&&po.X < CenterX)
						a = 90 + (90 + a);
					if (po.Y < CenterY&&po.X < CenterX)
						a += 180;
					if (po.Y < CenterY&&po.X>CenterX)
						a = 270 + (90 + a);

					a -= SmallCircleAngle / 2;
					if (LastBeginAngle != 0)
					{
						if (a < (LastBeginAngle + SmallCircleAngle))
						{
							float l = (LastBeginAngle + SmallCircleAngle) - a;
							int x = CenterX + ((PieWidth - (PieWidth - HollowWidth) / 2) / 2) * cos(-(nSweep + l + abs(nTemp)/* / 2.f*/)* pi / 180.f);
							int y = CenterY - ((PieWidth - (PieWidth - HollowWidth) / 2) / 2) * sin(-(nSweep + l + abs(nTemp)/* / 2.f*/)* pi / 180.f);

							CircleRect.X = x - m_SmallCircle / 2;
							CircleRect.Y = y - m_SmallCircle / 2;
							CircleRect.Height = CircleRect.Width = m_SmallCircle;
						}
					}
					LastBeginAngle = a;

					Gdiplus::SolidBrush hBrush(Gdiplus::Color(GetAValue(m_VecPies[i].Color), GetRValue(m_VecPies[i].Color), GetGValue(m_VecPies[i].Color), GetBValue(m_VecPies[i].Color)));
					hBrush.SetColor(Gdiplus::Color(GetAValue(m_VecPies[i].Color), GetRValue(m_VecPies[i].Color), GetGValue(m_VecPies[i].Color), GetBValue(m_VecPies[i].Color)));
					graph.FillEllipse(&hBrush, CircleRect);


					CRect rect;
					rect.top = CircleRect.GetTop();
					rect.left = CircleRect.GetLeft();
					rect.bottom = CircleRect.GetBottom();
					rect.right = CircleRect.GetRight();
					SStringT str;
					str.Format(SStringT().Format(L"%%0.%df", m_Decimal), m_VecPies[i].Value);
					pRT->DrawText(str, str.GetLength(), rect, DT_CENTER | DT_VCENTER | DT_SINGLELINE);
					nSweep += nTemp;
				}
			}
		}

		//响应鼠标显示提示框
		if (m_CursorTip&&m_CursorPoint.x > 0 && m_CursorPoint.y > 0)
		{
			m_CursorInPie = false;
			float angle = atan((m_CursorPoint.y - CenterY) / (m_CursorPoint.x - CenterX));//弧度
			float theta = angle*(180 / pi);//角度

			if (m_CursorPoint.y > CenterY&&m_CursorPoint.x < CenterX)
				theta = 90 + (90 + theta);
			if (m_CursorPoint.y < CenterY&&m_CursorPoint.x < CenterX)
				theta += 180;
			if (m_CursorPoint.y < CenterY&&m_CursorPoint.x>CenterX)
				theta = 270 + (90 + theta);

			float rr = sqrt(pow(CenterX - m_CursorPoint.x, 2) + pow(CenterY - m_CursorPoint.y, 2));

			for (size_t n = 0; n < _Args.size(); n++)
			{
				_Arg &a = _Args[n];
				if (rr<a.MaxR&&rr>(HollowWidth / 2) && a.BeginArg<theta&&a.EndArg>theta)
				{

					//准备绘制提示框
					SArray<SStringT> TipTexts;
					TipTexts.Add(m_SeriesName);
					SStringT str;
					str.Format(SStringT().Format(L"%%s:%%.%df(%%.2f%%%%)", m_Decimal), m_VecPies[n].StrTip, m_VecPies[n].Value, (a.EndArg - a.BeginArg) / 360 * 100);
					TipTexts.Add(str);

					//计算出矩形大小
					CRect rect;
					int MaxTextWidth = 0;
					int MaxTextHeight = 0;
					SIZE size;
					for (int i = 0; i < TipTexts.GetCount(); i++)
					{
						if (TipTexts[i].GetLength()>0)
						{
							pRT->MeasureText(TipTexts[i], TipTexts[i].GetLength(), &size);
							MaxTextWidth = max(MaxTextWidth, size.cx);
							MaxTextHeight += size.cy;
						}
					}

					rect.top = m_CursorPoint.y + TIP_OFFSET;
					rect.left = m_CursorPoint.x + TIP_OFFSET;
					rect.bottom = rect.top + MaxTextHeight + TIP_MARGIN * 2;
					rect.right = rect.left + MaxTextWidth + TIP_MARGIN * 2;


					//调整提示框位置
					if (rect.bottom > rcClient.bottom) rect.OffsetRect(CPoint(0, -(rect.bottom - rcClient.bottom)));
					if (rect.right > rcClient.right) rect.OffsetRect(CPoint(-(rect.right - rcClient.right), 0));

					//绘制提示框
					pRT->FillSolidRoundRect(rect, CPoint(2, 2), m_CursorTipColorBkgnd);
					if (m_CursorTipFont.GetFontPtr() != NULL)	pRT->SelectObject(m_CursorTipFont.GetFontPtr());
					pRT->SetTextColor(m_CursorTipTextColor);

					rect.left += TIP_MARGIN;
					rect.top += TIP_MARGIN;
					//绘制提示框中的文字
					for (size_t i = 0; i < TipTexts.GetCount(); i++)
					{
						if (TipTexts[i].GetLength()>0)
						{
							pRT->DrawText(TipTexts[i], TipTexts[i].GetLength(), rect, DT_LEFT | DT_VCENTER /*| DT_SINGLELINE*/);
							rect.OffsetRect(0, size.cy);
						}
					}
					break;
				}
			}
		}

		pRT->ReleaseDC(hDC);

		pRT->SelectObject(curBrush);
		pRT->SelectObject(curPen);
		pRT->SelectObject(curFont);
		pRT->SetTextColor(curTextColor);

		AfterPaint(pRT, painter);
	}

	void SPieChart::OnMouseMove(UINT nFlags, CPoint point)
	{
		if (m_CursorTip)
		{
			m_CursorPoint = point;
			InvalidateRect(&GetClientRect(), false);
			if (m_CursorInPie)
				SetCursor(LoadCursor(NULL, IDC_HAND));
			else
				SetCursor(LoadCursor(NULL, IDC_ARROW));
		}
		return;

	}

	void SPieChart::OnMouseLeave()
	{
		if (m_CursorTip)
		{
			m_CursorPoint.x = m_CursorPoint.y = 0;
			InvalidateRect(&GetClientRect());
		}
	}

	void SPieChart::SetSeriesName(LPCTSTR Name)
	{
		m_SeriesName = Name;
	}
}