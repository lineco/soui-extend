#include "stdafx.h"
#include "SBarGradualChart.h"
#pragma comment(lib,"gdiplus")

namespace SOUI
{

	CGraphicsRoundRectPath::CGraphicsRoundRectPath(void)
		:Gdiplus::GraphicsPath()
	{
	}

	CGraphicsRoundRectPath::CGraphicsRoundRectPath(INT x, INT y, INT width, INT height, INT cornerX, INT cornerY, CircularBeadPo FilletBead)
		: Gdiplus::GraphicsPath()
	{
		AddRoundRect(x, y, width, height, cornerX, cornerY, FilletBead);
	}

	void CGraphicsRoundRectPath::AddRoundRect(INT x, INT y, INT width, INT height, INT cornerX, INT cornerY, CircularBeadPo FilletBead)
	{
		INT elWid = 2 * cornerX;
		INT elHei = 2 * cornerY;




		if (FilletBead.LeftTop)
		{
			AddArc(x, y, elWid, elHei, 180, 90); // 左上角圆弧
			AddLine(x + cornerX, y, x + width - (FilletBead.RightTop ? cornerX : 0), y); // 上边
		}
		else
			AddLine(x, y, x + width, y); // 上边


		if (FilletBead.RightTop)
		{
			AddArc(x + width - elWid, y, elWid, elHei, 270, 90); // 右上角圆弧
			AddLine(x + width, y + cornerY, x + width, y + height - (FilletBead.RightBottom ? cornerY : 0));// 右边
		}
		else
			AddLine(x + width, y, x + width, y + height);// 右边


		if (FilletBead.RightBottom)
		{
			AddArc(x + width - elWid, y + height - elHei, elWid, elHei, 0, 90); // 右下角圆弧
			AddLine(x + width - cornerX, y + height, x + (FilletBead.LeftBottom ? cornerX : 0), y + height); // 下边
		}
		else
			AddLine(x + width, y + height, x, y + height); // 下边

		if (FilletBead.LeftBottom)
		{
			AddArc(x, y + height - elHei, elWid, elHei, 90, 90);
			AddLine(x, y + (FilletBead.LeftTop ? cornerY : 0), x, y + height - cornerY);//左边
		}
		else;
		AddLine(x, y, x, y + height);//左边

	}


	SBarGradualChart::SBarGradualChart() :
		m_BarWidth(0.2),
		m_BarColor1(DEF_BAR_COLOR1),
		m_BarColor2(DEF_BAR_COLOR2),
		m_BarInterval(0.4),
		m_ShowValue(true),
		m_Unit(_T("")),
		m_BarTextColor(RGBA(0, 0, 0, 0)),
		m_Fillet(0)
	{
		m_PullOver = false;
		m_GradualRange.x = m_GradualRange.y = 0;



	}


	SBarGradualChart::~SBarGradualChart()
	{

	}

	void SBarGradualChart::AddData(int id, float Value, float Angle, bool IsShowValue)
	{
		if (m_Data[id].GetCount() > 0)
			AddData(id, Value, m_Data[id][m_Data[id].GetCount() - 1].BarColor1, m_Data[id][m_Data[id].GetCount() - 1].BarColor2, m_Data[id][m_Data[id].GetCount() - 1].Angle, IsShowValue);
		else
			AddData(id, Value, m_BarColor1, m_BarColor2, Angle, IsShowValue);
	}

	void SBarGradualChart::AddData(int id, float Value, COLORREF BarColor1, COLORREF BarColor2, float Angle, bool IsShowValue)
	{
		BarDataType t;
		t.Value = Value;
		t.BarColor1 = BarColor1;
		t.BarColor2 = BarColor2;
		t.Angle = Angle;
		t.IsShowValue = IsShowValue;
		m_Data[id].Add(t);
	}



	void SBarGradualChart::Clear(void)
	{
		m_Data.RemoveAll();
	}

	bool SBarGradualChart::Remove(int id)
	{
		return m_Data.RemoveKey(id);
	}

	void SBarGradualChart::OnPaint(IRenderTarget *pRT)
	{
		__super::OnPaint(pRT);
		SPainter painter;
		BeforePaint(pRT, painter);
		HDC hdc = pRT->GetDC();
		Gdiplus::Graphics gps(hdc);
		gps.SetSmoothingMode(Gdiplus::SmoothingModeHighQuality);

		CAutoRefPtr<IRenderObj> curBrush = pRT->GetCurrentObject(OT_BRUSH);
		CAutoRefPtr<IRenderObj> curPen = pRT->GetCurrentObject(OT_PEN);
		CAutoRefPtr<IRenderObj> curFont = pRT->GetCurrentObject(OT_FONT);
		COLORREF curTextColor = pRT->GetTextColor();

		SOUI::SPOSITION pos = m_Data.GetStartPosition();
		float BarMaxWidth;
		int BarTypeCount = m_Data.GetCount();
		int BarWidth = 0;
		int BarInterval = 0;
		int index = 0;
		while (pos&&!m_PullOver)
		{
			CoordDataType::CPair *p = m_Data.GetNext(pos);
			for (size_t i = 0; i < p->m_value.GetCount(); i++)
			{
				CRect Bar;

				if (m_AxisType == AXIS_LENGTHWAYS)
				{
					BarInterval = m_BarInterval <= 1 ? m_XSignRatio / 2 * m_BarInterval : m_BarInterval;
					BarMaxWidth = m_XSignRatio - BarInterval;

					float SingleMaxWidth = (BarMaxWidth / (float)BarTypeCount);
					BarWidth = m_BarWidth <= 1 ? SingleMaxWidth*m_BarWidth : m_BarWidth;
					if (BarWidth > SingleMaxWidth)
						BarWidth = SingleMaxWidth;
					float SingleOffset = m_XSignRatio*(i)+m_XSignRatio / 2 + ((SingleMaxWidth*index) + SingleMaxWidth / 2) + BarInterval / 2;//单个柱中心位置

					Bar.left = GetDataDrawRect().left + SingleOffset - BarWidth / 2;
					Bar.right = Bar.left + BarWidth;
					Bar.top = GetDataDrawRect().bottom - (m_ValueRatio*(p->m_value[i].Value - m_MinValue));
					Bar.bottom = GetDataDrawRect().bottom;


					//圆角矩形支持
					POINT po;
					po.x = po.y = m_Fillet < 1 ? m_Fillet*(Bar.Width() / 2) : m_Fillet;
					po.x = po.x = po.x>(Bar.Width() / 2) ? Bar.Width() / 2 : po.x;//范围限制

					/*Updata*/
					Gdiplus::Color C1(GetAValue(p->m_value[i].BarColor1), GetRValue(p->m_value[i].BarColor1), GetGValue(p->m_value[i].BarColor1), GetBValue(p->m_value[i].BarColor1));
					Gdiplus::Color C2(GetAValue(p->m_value[i].BarColor2), GetRValue(p->m_value[i].BarColor2), GetGValue(p->m_value[i].BarColor2), GetBValue(p->m_value[i].BarColor2));
					Gdiplus::RectF Rectf;
					Rectf.X = m_GradualRange.x ? Bar.left : m_DataRect.left;
					Rectf.Y = m_GradualRange.y ? Bar.top : m_DataRect.top;
					Rectf.Width = m_GradualRange.x ? Bar.Width() : m_DataRect.Width();
					Rectf.Height = m_GradualRange.y ? Bar.Height() : m_DataRect.Height();
					Gdiplus::LinearGradientBrush blackBrush(Rectf, C1, C2, p->m_value[i].Angle, false);
					CGraphicsRoundRectPath RoundRectPath;//创建圆角矩形路径对象

					if (m_FilletPoint.IsEmpty())
						RoundRectPath.AddRoundRect(Bar.left, Bar.top, Bar.Width(), Bar.Height(), po.x, po.y, CircularBeadPo(true, true, false, false));
					else
						RoundRectPath.AddRoundRect(Bar.left, Bar.top, Bar.Width(), Bar.Height(), po.x, po.y, m_FilletPoint);

					gps.FillPath(&blackBrush, &RoundRectPath);


					if (m_ShowValue&&p->m_value[i].IsShowValue)
					{
						SStringT str;
						SIZE size;
						NumberToScaleStr(p->m_value[i].Value, m_Decimal, str);
						str += m_Unit;
						if (m_BarTextFont.GetFontPtr() != NULL)	pRT->SelectObject(m_BarTextFont.GetFontPtr());
						if (m_BarTextColor != RGBA(0, 0, 0, 0)) pRT->SetTextColor(m_BarTextColor);
						pRT->MeasureText(str, str.GetLength(), &size);
						pRT->TextOut(Bar.CenterPoint().x - size.cx / 2, Bar.top - size.cy, str, -1);
					}
				}
				else if (m_AxisType == AXIS_CROSSWISE)
				{
					BarInterval = m_BarInterval <= 1 ? m_YSignRatio / 2 * m_BarInterval : m_BarInterval;
					BarMaxWidth = m_YSignRatio - BarInterval;

					float SingleMaxWidth = (BarMaxWidth / (float)BarTypeCount);
					BarWidth = m_BarWidth <= 1 ? SingleMaxWidth *m_BarWidth : m_BarWidth;
					if (BarWidth > SingleMaxWidth)
						BarWidth = SingleMaxWidth;
					float SingleOffset = m_YSignRatio*(i)+m_YSignRatio / 2 + ((SingleMaxWidth*index) + SingleMaxWidth / 2) + BarInterval / 2;//单个柱中心位置

					Bar.top = GetDataDrawRect().top + SingleOffset - BarWidth / 2;
					Bar.bottom = Bar.top + BarWidth;
					Bar.left = GetDataDrawRect().left;
					Bar.right = GetDataDrawRect().left + (m_ValueRatio*(p->m_value[i].Value - m_MinValue));

					//圆角矩形支持
					POINT po;
					po.x = po.y = m_Fillet < 1 ? m_Fillet*(Bar.Height() / 2) : m_Fillet;
					po.x = po.x = po.x>(Bar.Height() / 2) ? Bar.Height() / 2 : po.x;//范围限制


					/*Updata*/
					Gdiplus::Color C1(GetAValue(p->m_value[i].BarColor1), GetRValue(p->m_value[i].BarColor1), GetGValue(p->m_value[i].BarColor1), GetBValue(p->m_value[i].BarColor1));
					Gdiplus::Color C2(GetAValue(p->m_value[i].BarColor2), GetRValue(p->m_value[i].BarColor2), GetGValue(p->m_value[i].BarColor2), GetBValue(p->m_value[i].BarColor2));
					Gdiplus::RectF Rectf;
					Rectf.X = m_GradualRange.x ? Bar.left : m_DataRect.left;
					Rectf.Y = m_GradualRange.y ? Bar.top : m_DataRect.top;
					Rectf.Width = m_GradualRange.x ? Bar.Width() : m_DataRect.Width();
					Rectf.Height = m_GradualRange.y ? Bar.Height() : m_DataRect.Height();
					Gdiplus::LinearGradientBrush blackBrush(Rectf, C1, C2, p->m_value[i].Angle, false);
					CGraphicsRoundRectPath RoundRectPath;//创建圆角矩形路径对象
					if (m_FilletPoint.IsEmpty())
						RoundRectPath.AddRoundRect(Bar.left, Bar.top, Bar.Width(), Bar.Height(), po.x, po.y, CircularBeadPo(false, true, true, false));
					else
						RoundRectPath.AddRoundRect(Bar.left, Bar.top, Bar.Width(), Bar.Height(), po.x, po.y, m_FilletPoint);
					gps.FillPath(&blackBrush, &RoundRectPath);

					if (m_ShowValue)
					{
						SStringT str;
						SIZE size;
						NumberToScaleStr(p->m_value[i].Value, m_Decimal, str);
						str += m_Unit;
						if (m_BarTextFont.GetFontPtr() != NULL)	pRT->SelectObject(m_BarTextFont.GetFontPtr());
						if (m_BarTextColor != RGBA(0, 0, 0, 0)) pRT->SetTextColor(m_BarTextColor);
						pRT->MeasureText(str, str.GetLength(), &size);
						pRT->TextOut(Bar.right, Bar.top + (Bar.Height() / 2 - size.cy / 2), str, -1);
					}
				}

			}
			index++;
		}
		pRT->SelectObject(curBrush);
		pRT->SelectObject(curPen);
		pRT->SelectObject(curFont);
		pRT->SetTextColor(curTextColor);

		pRT->ReleaseDC(hdc);
		AfterPaint(pRT, painter);
	}



	void SBarGradualChart::GetMaxMin(float &Max, float &Min)
	{
		SOUI::SPOSITION pos = m_Data.GetStartPosition();
		bool flag = false;
		while (pos)
		{
			CoordDataType::CPair *p = m_Data.GetNext(pos);
			for (size_t i = 0; i < p->m_value.GetCount(); i++)
			{
				float TempValue = p->m_value[i].Value;
				if (i == 0 && !flag)
				{
					Max = Min = TempValue;
					flag = true;
				}

				if (Min > TempValue)
					Min = TempValue;
				if (Max < TempValue)
					Max = TempValue;
			}
		}
	}

	void SBarGradualChart::SetUnit(SStringT Unit)
	{
		m_Unit = Unit;
	}

	void SBarGradualChart::ShowValue(bool Show)
	{
		m_ShowValue = Show;
	}

	void SBarGradualChart::SetBarInterval(float Interval)
	{
		m_BarInterval = Interval;
	}

	void SBarGradualChart::SetBarWidth(float Width)
	{
		m_BarWidth = Width;
	}

	void SBarGradualChart::SetData(int id, SArray<BarDataType> &Data)
	{
		m_Data[id] = Data;
	}

	SArray<SBarGradualChart::BarDataType> &SBarGradualChart::GetData(int id)
	{
		return m_Data[id];
	}


	HRESULT SBarGradualChart::OnAttrtFilletPoint(const SStringW& strValue, BOOL bLoading)
	{
		int c1, c2, c3, c4;
		swscanf_s(strValue, L"%d,%d,%d,%d", &c1, &c2, &c3, &c4);
		m_FilletPoint.LeftTop = (int)c1;
		m_FilletPoint.RightTop = (int)c2;
		m_FilletPoint.RightBottom = (int)c3;
		m_FilletPoint.LeftBottom = (int)c4;
		return bLoading ? S_FALSE : S_OK;
	}

}