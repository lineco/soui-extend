// MainDlg.cpp : implementation of the CMainDlg class
//
/////////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "MainDlg.h"	

	
CMainDlg::CMainDlg() : SHostWnd(_T("LAYOUT:XML_MAINWND"))
{
	m_bLayoutInited = FALSE;
}

CMainDlg::~CMainDlg()
{
}

int CMainDlg::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	SetMsgHandled(FALSE);
	return 0;
}

BOOL CMainDlg::OnInitDialog(HWND hWnd, LPARAM lParam)
{
	m_bLayoutInited = TRUE;
	srand(time(NULL));


	SLineChart *pLine1 = FindChildByName2<SLineChart>(L"Line1");
	pLine1->CreateLine(0,L"母猪数量",RGBA(255,0,0,255));
	for (int i = 0; i < 10; i++)
	{
		SStringT str;
		pLine1->AddText(str.Format(L"%d天",i));
		pLine1->AddData(0, rand() % 100);
	}


	SLineChart *pLine2 = FindChildByName2<SLineChart>(L"Line2");
	pLine2->CreateLine(0, L"母猪数量", RGBA(255, 0, 0, 255));
	pLine2->CreateLine(1, L"公猪数量", RGBA(0, 255, 0, 255));
	for (int i = 0; i < 10; i++)
	{
		SStringT str;
		pLine2->AddText(str.Format(L"%d天", i));
		pLine2->AddData(0, rand() % 100);
		pLine2->AddData(1, rand() % 100);
	}

	SLineChart *pLine3 = FindChildByName2<SLineChart>(L"Line3");
	pLine3->CreateLine(0, L"母猪数量", RGBA(255, 0, 0, 255), RGBA(255, 0, 0, 128), RGBA(128,128, 0, 128));
	pLine3->CreateLine(1, L"公猪数量", RGBA(0, 255, 0, 255));
	for (int i = 0; i < 10; i++)
	{
		SStringT str;
		pLine3->AddText(str.Format(L"%d天", i));
		pLine3->AddData(0, rand() % 100);
		pLine3->AddData(1, rand() % 100);
	}

	SLineChart *pLine4 = FindChildByName2<SLineChart>(L"Line4");
	pLine4->CreateLine(0, L"母猪数量", RGBA(255, 0, 0, 255));
	for (int i = 0; i < 10; i++)
	{
		SStringT str;
		pLine4->AddText(str.Format(L"%d天", i));
		pLine4->AddData(0, rand() % 100);
	}

	SLineChart *pCurve[4];
	pCurve[0]= FindChildByName2<SLineChart>(L"Curve1");
	pCurve[1] = FindChildByName2<SLineChart>(L"Curve2");
	pCurve[2] = FindChildByName2<SLineChart>(L"Curve3");
	pCurve[3] = FindChildByName2<SLineChart>(L"Curve4");

	pCurve[0]->CreateLine(0, L"母猪数量", RGBA(255, 0, 0, 255));
	for (int i = 0; i < 10; i++)
	{
		SStringT str;
		pCurve[0]->AddText(str.Format(L"%d天", i));
		pCurve[0]->AddData(0, rand() % 100, str);
	}

	pCurve[1]->CreateLine(0, L"母猪数量", RGBA(255, 0, 0, 255));
	pCurve[1]->CreateLine(1, L"公猪数量", RGBA(0, 255, 0, 255));
	for (int i = 0; i < 10; i++)
	{
		SStringT str;
		pCurve[1]->AddText(str.Format(L"%d天", i));
		pCurve[1]->AddData(0, rand() % 100, str);
		pCurve[1]->AddData(1, rand() % 100, str);
	}


	pCurve[2]->CreateLine(0, L"母猪数量", RGBA(255, 0, 0, 255),  RGBA(200, 0, 0, 128), RGBA(128, 128, 0, 128));
	//pCurve[2]->CreateLine(1, L"公猪数量", RGBA(0, 0, 0, 255), RGBA(200,200, 0, 128), RGBA(0,0,200, 128));
	pCurve[2]->SetLineWidth(2);
	pCurve[2]->SetDataEnd(0.8);

	pCurve[2]->AddText(SStringW(L"1天"));
	pCurve[2]->AddData(0,100);
	pCurve[2]->AddText(SStringW(L"2天"));
	pCurve[2]->AddData(0, 0);
	pCurve[2]->AddText(SStringW(L"3天"));
	pCurve[2]->AddData(0, 0);
	pCurve[2]->AddText(SStringW(L"4天"));
	pCurve[2]->AddData(0, 0);
	pCurve[2]->AddText(SStringW(L"5天"));
	pCurve[2]->AddData(0, 0);
	pCurve[2]->AddText(SStringW(L"6天"));
	pCurve[2]->AddData(0, 50);
	pCurve[2]->AddText(SStringW(L"7天"));
	pCurve[2]->AddData(0, 50);
	pCurve[2]->AddText(SStringW(L"8天"));
	pCurve[2]->AddData(0, 0);
	pCurve[2]->AddText(SStringW(L"9天"));
	pCurve[2]->AddData(0, 0);



	pCurve[3]->CreateLine(0, L"母猪数量", RGBA(255, 0, 0, 255));
	for (int i = 0; i < 10; i++)
	{
		SStringT str;
		pCurve[3]->AddText(str.Format(L"%d天", i));
		pCurve[3]->AddData(0, rand() % 100);
	}


	SBarChart *pBar[4];
	pBar[0] = FindChildByName2<SBarChart>(L"bar1");
	pBar[1] = FindChildByName2<SBarChart>(L"bar2");
	pBar[2] = FindChildByName2<SBarChart>(L"bar3");
	pBar[3] = FindChildByName2<SBarChart>(L"bar4");

	pBar[0]->SetBarWidth(0.8);
	for (int i = 0; i < 10; i++)
	{
		SStringT str;
		pBar[0]->AddText(str.Format(L"%d天", i));
		pBar[1]->AddText(str.Format(L"%d天", i));
		pBar[2]->AddText(str.Format(L"%d天", i));
		pBar[3]->AddText(str.Format(L"%d天", i));

		pBar[0]->AddData(0, rand() % 100,RGBA(255,0,0,255));

		pBar[1]->AddData(0, rand() % 100, RGBA(255,0, 0, 255));
		pBar[1]->AddData(1, rand() % 100, RGBA(rand()%255, rand()%255, rand()%255, 255));

		pBar[2]->AddData(0, rand() % 100, RGBA(255,255, 0, 255));
		pBar[2]->AddData(1, rand() % 100, RGBA(0,255,255, 255));

		pBar[3]->AddData(0, rand() % 100, RGBA(255, 255, 255, 255));
	}

	SPieChart *pPie[4];
	pPie[0] = FindChildByName2<SPieChart>(L"pie1");
	pPie[1] = FindChildByName2<SPieChart>(L"pie2");
	pPie[2] = FindChildByName2<SPieChart>(L"pie3");
	pPie[3] = FindChildByName2<SPieChart>(L"pie4");

	for (int n = 0; n < 4;n++)
	{
		pPie[n]->SetSeriesName(L"母猪数量");
		for (int i = 0; i < 6; i++)
		{
			SStringT str;
			pPie[n]->AddPie(PIE(rand() % 100, RGBA(rand() % 255, rand() % 255, rand() % 255,255),str.Format(L"%d天",i)));
		}
	}
	SetTimer(TIMER_MSG,500);


	SPieChart *pPie1 = FindChildByName2<SPieChart>(L"pie");
	pPie1->SetSeriesName(L"母猪数量");
	//for (int i = 0; i < 6; i++)
	//{
	//	SStringT str;
	//	pPie1->AddPie(PIE(rand() % 100, RGBA(rand() % 255, rand() % 255, rand() % 255, 255), str.Format(L"%d天", i)));
	//}
	SStringT str;
	pPie1->AddPie(PIE(40, RGBA(rand() % 255, rand() % 255, rand() % 255, 255), str));
	pPie1->AddPie(PIE(1, RGBA(rand() % 255, rand() % 255, rand() % 255, 255), str));
	pPie1->AddPie(PIE(4, RGBA(rand() % 255, rand() % 255, rand() % 255, 255), str));
	pPie1->AddPie(PIE(5, RGBA(rand() % 255, rand() % 255, rand() % 255, 255), str));
	pPie1->AddPie(PIE(3, RGBA(rand() % 255, rand() % 255, rand() % 255, 255), str));

	SEllipseChart *pEllipse = FindChildByName2<SEllipseChart>(L"Ellipse");
	for (int i = 0; i < 5; i++)
	{
		SStringT str;
		pEllipse->AddText(str.Format(L"%d天", i));
		int r = rand() % 255;
		int g = rand() % 255;
		int b = rand() % 255;
		COLORREF Color = RGBA(r, g, b, 255);
		COLORREF ColorEnd = RGBA(r, g, b, 50);

		pEllipse->AddData(i, SEllipseChart::EllipseDataType(rand() % 100, 0.1f*(rand()%10), Color, ColorEnd));
	}

	SBarContrastChart *pSBarContrastChart = FindChildByName2<SBarContrastChart>(L"BarContrast");
	for (int i = 0; i < 5; i++)
	{
		SStringT str;
		pSBarContrastChart->AddBar(
			SBarContrastChart::BarAttribute(str.Format(L"%d天", i),0,L"头",0, 100, \
			SBarContrastChart::Bar_s(rand() % 100, RGBA(88, 255, 240, 255), RGBA(88, 255, 240, 50),0.5,true),
			SBarContrastChart::Bar_s(rand() % 100, RGBA(88, 255, 240, 255), RGBA(88, 255, 240, 50),0.5, true)));
	}
	//
	SLegend* pSLegend = FindChildByName2<SLegend>(L"legend");
	for (int i = 0; i < 5; i++)
	{
		int r = rand() % 255;
		int g = rand() % 255;
		int b = rand() % 255;
		pSLegend->Add(Legend(SStringW().Format(L"%d天", i),RGBA(r,g,b,255)));
	}
	pSLegend->Setxy(4,2,true);

	SChinaMap* pMap = FindChildByName2<SChinaMap>(L"map_1");

	pMap->Add(SChinaMap::SplashesData(L"西安市", 150));
	pMap->Add(SChinaMap::SplashesData(L"合肥市", 150));
	pMap->Add(SChinaMap::SplashesData(L"成都市", 50));
	pMap->Add(SChinaMap::SplashesData(L"北京市", 50));
	pMap->Add(SChinaMap::SplashesData(L"上海市", 50));
	pMap->Add(SChinaMap::SplashesData(L"乌鲁木齐市", 60));
	pMap->Add(SChinaMap::SplashesData(L"齐齐哈尔市", 60));
	pMap->Add(SChinaMap::SplashesData(L"海口市", 60));
	pMap->Add(SChinaMap::SplashesData(L"大兴安岭地区", 50));




	for (int i = 1; i <= 2; i++)
	{
		SBarGradualChart *pBarGradualChart = FindChildByName2<SBarGradualChart>(SStringW().Format(L"BarGradualChart_%d", i));
		SLegend* pSLegend2 = FindChildByName2<SLegend>(SStringW().Format(L"legend_%d", i));
		COLORREF Color1 = DEF_BAR_COLOR2;
		COLORREF Color2 = RGBA(7, 178, 51, 255);
		for (int i = 0; i < 10; i++)
		{
			pBarGradualChart->AddText(str.Format(L"%d天", i));
			pBarGradualChart->AddData(0, rand() % 100, DEF_BAR_COLOR1, Color1, 90);
			pBarGradualChart->AddData(1, rand() % 100, RGBA(128, 178, 141, 255), Color2, 90);
		}
		pBarGradualChart->SetDecimal(1);
		pSLegend2->Add(Legend(SStringW().Format(L"母猪出栏数量", 0), Color1));
		pSLegend2->Add(Legend(SStringW().Format(L"公猪出栏数量", 1), Color2));
		pSLegend2->Setxy(2, 1, true);
	}

	
	SOUI::SLineBarChart *pLineBarChart = FindChildByName2<SOUI::SLineBarChart>(L"LineBarChart_test");
	if(pLineBarChart)
	{
		for (int i = 0; i < 6; i++)
		{
			SStringT str;
			pLineBarChart->AddText(str.Format(L"%d天", i));
			pLineBarChart->AddData(0,0,RGBA(255,0,0,255));
			pLineBarChart->AddData(1, rand() % 100,RGBA(0,255,0,255));
			pLineBarChart->AddData(2, rand() % 100,RGBA(0,0,158,255));

			pLineBarChart->AddLineData(0,rand()/(RAND_MAX+1.0),RGBA(255,0,0,255));
			pLineBarChart->AddLineData(1,rand()/(RAND_MAX+1.0),RGBA(0,255,0,255));
		}
	}

	return 0;
}

void CMainDlg::OnTimer(UINT_PTR idEvent)
{

	if (idEvent == TIMER_MSG)
	{
		SLineChart *pLine1 = FindChildByName2<SLineChart>(L"Line1");
		pLine1->PopTopPushBack(0, rand() % 100);
		pLine1->UpdateWindow();

		//SLineChart *pCurve1 = FindChildByName2<SLineChart>(L"Curve1");
		//pCurve1->PopTopPushBack(0, rand() % 100);
		//pCurve1->UpdateWindow();

		SBarChart *pBar = FindChildByName2<SBarChart>(L"bar1");

		pBar->GetData(0)[0].Value;

	}
	else
		SetMsgHandled(FALSE);
}

//TODO:消息映射
void CMainDlg::OnClose()
{

	CSimpleWnd::DestroyWindow();
}
void CMainDlg::OnDestroy()
{
	SetMsgHandled(FALSE);
	KillTimer(TIMER_MSG);
}

void CMainDlg::OnMaximize()
{
	SendMessage(WM_SYSCOMMAND, SC_MAXIMIZE);
}
void CMainDlg::OnRestore()
{
	SendMessage(WM_SYSCOMMAND, SC_RESTORE);
}
void CMainDlg::OnMinimize()
{
	SendMessage(WM_SYSCOMMAND, SC_MINIMIZE);
}

void CMainDlg::OnSize(UINT nType, CSize size)
{
	SetMsgHandled(FALSE);
	if (!m_bLayoutInited) return;
	
	SWindow *pBtnMax = FindChildByName(L"btn_max");
	SWindow *pBtnRestore = FindChildByName(L"btn_restore");
	if(!pBtnMax || !pBtnRestore) return;
	
	if (nType == SIZE_MAXIMIZED)
	{
		pBtnRestore->SetVisible(TRUE);
		pBtnMax->SetVisible(FALSE);
	}
	else if (nType == SIZE_RESTORED)
	{
		pBtnRestore->SetVisible(FALSE);
		pBtnMax->SetVisible(TRUE);
	}
}

