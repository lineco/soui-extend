/**
* Copyright (C) 2019-2050
* All rights reserved.
*
* @file		  SLegend.h
* @brief
* @version    v1.0
* @author     yiywain(逸远)
* @date       2021-03-19
* gitee  https://gitee.com/yiywain/soui-extend.git
* Describe   标签类
*/

#pragma once
#include <vector>

namespace SOUI
{

enum LegendCheckTypeEnum{
	LegendCheckType_Ellipse,//圆形
	LegendCheckType_Triangle,//三角形
	LegendCheckType_Rectangle,//矩形
	LegendCheckType_RoundedRectangle //圆角矩形
};


class SLegend;
class SLegendCheckBox : public	SWindow
{
	friend SLegend;
	SOUI_CLASS_NAME(SLegendCheckBox, L"legendCheck")
public:
	SLegendCheckBox();
	virtual ~SLegendCheckBox();

	enum
	{
		CheckBoxSpacing=5
	};

protected:
	void OnPaint(IRenderTarget *pRT);
	/**
	* SCheckBox::GetDesiredSize
	* @brief    获取预期大小
	* @param    LPRECT pRcContainer  --  内容矩形框
	* @return   返回值 CSize对象
	*
	* Describe  根据矩形的大小，获取预期大小(解释有点不对)
	*/
	virtual CSize GetDesiredSize(LPCRECT pRcContainer);
	/**
	* SCheckBox::GetTextRect
	* @brief    获取文本大小
	* @param    LPRECT pRect  --  内容矩形框
	*
	* Describe  设置矩形的大小
	*/
	virtual void GetTextRect(LPRECT pRect);

	void SetBoxColor(COLORREF Color);

	COLORREF GetBoxColor(void);
private:
	
protected:
	SOUI_MSG_MAP_BEGIN()
		MSG_WM_CREATE(OnCreate)
		MSG_WM_PAINT_EX(OnPaint)
	SOUI_MSG_MAP_END()

	SOUI_ATTRS_BEGIN()
		//ATTR_RECT(L"CoordMargin", m_CoordMargin,TRUE)
		ATTR_INT(L"legendSize",m_LegendSize,TRUE)
		ATTR_COLOR(L"colorBox",m_Color,RGBA(255,0,0,255))
		ATTR_ENUM_BEGIN(L"type", LegendCheckTypeEnum, TRUE)
		ATTR_ENUM_VALUE(L"roundness", LegendCheckType_Ellipse)
			ATTR_ENUM_VALUE(L"triangle", LegendCheckType_Triangle)
			ATTR_ENUM_VALUE(L"rectangle", LegendCheckType_Rectangle)
			ATTR_ENUM_VALUE(L"roundedRectangle", LegendCheckType_RoundedRectangle)
		ATTR_ENUM_END(m_Type)
	SOUI_ATTRS_END()

protected:
	LegendCheckTypeEnum m_Type;
	COLORREF m_Color;
	int m_LegendSize;
};

enum LayoutTypeEnum
{
	LayoutTypeEnum_Fixation,
	LayoutTypeEnum_Self_Adaption
};

struct Legend{
	SStringT Text;
	COLORREF Color;
	SLegendCheckBox *pLegend;
	Legend()
	{
		Color = RGBA(255,0,0,255);
		pLegend = NULL;
	}
	Legend(SStringT Text, COLORREF Color)
	{
		this->Text = Text;
		this->Color = Color;
		pLegend = NULL;
	}

};


class SLegend : public	SWindow
{
	SOUI_CLASS_NAME(SLegend, L"legend")
public:
	SLegend();
	virtual ~SLegend();

	void Add(Legend &Legend);
	int Size(void);
	void Clear(void);
	Legend *GetAt(int i);

	//设置X方向和Y方向的数量
	void Setxy(int x,int y,bool IsUpdate=false);
	void UpdateWindow(void);

protected:
	int OnCreate(LPCREATESTRUCT lpCreateStruct);
	void OnPaint(IRenderTarget *pRT);

	void AddLegendChild(Legend &Legend);

//	void SyncWindowToData(void);

private:
	int MeasureText(IRenderTarget *pRT, SStringT Text, int TextHeight, SIZE *size);

	void LayoutFixation();
protected:
	SOUI_MSG_MAP_BEGIN()
		MSG_WM_CREATE(OnCreate)
//		MSG_WM_PAINT_EX(OnPaint)
	SOUI_MSG_MAP_END()

	SOUI_ATTRS_BEGIN()
		ATTR_INT(L"xNumber", m_xNumber, TRUE)
		ATTR_INT(L"yNumber", m_yNumber, TRUE)
		ATTR_INT(L"xSpace", m_xSpace, TRUE)
		ATTR_INT(L"ySpace", m_ySpace, TRUE)
		ATTR_ENUM_BEGIN(L"type", LayoutTypeEnum, TRUE)
			ATTR_ENUM_VALUE(L"fixation", LayoutTypeEnum_Fixation)//当类型为LayoutTypeEnum_Fixation时 xNumber有效
			ATTR_ENUM_VALUE(L"self-adaption", LayoutTypeEnum_Self_Adaption)
		ATTR_ENUM_END(m_LayoutType)

	SOUI_ATTRS_END()

protected:
	LayoutTypeEnum m_LayoutType;
	int m_xNumber;
	int m_yNumber;
	int m_xSpace;
	int m_ySpace;
private:
	std::vector<Legend> m_Data;
};
}