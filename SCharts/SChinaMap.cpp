#include "stdafx.h"
#include "SChinaMap.h"
#include <helper/mybuffer.h>

//中国地图的上下左右经纬度
#define MAP_POINT_LEFT  73.52f
#define MAP_POINT_TOP  53.56f
#define MAP_POINT_RIGHT  135.09f
#define MAP_POINT_BOTTOM  18.23f

namespace SOUI
{

	SChinaMap::SChinaMap() :
		m_HeatPoMaxSize(0.08),
		m_HeatPoMinSize(0.02),
		m_ShowCity(true),
		m_MinAmend(0.03),
		m_MaxAmend(0.15)
	{


	}

	SChinaMap::~SChinaMap()
	{


	}

	void SChinaMap::OnPaint(IRenderTarget *pRT)
	{
		__super::OnPaint(pRT);
		SPainter painter;
		BeforePaint(pRT, painter);
		if (m_Data.GetCount() > 1)
		{
			CAutoRefPtr<IRenderObj> curBrush = pRT->GetCurrentObject(OT_BRUSH);
			CAutoRefPtr<IRenderObj> curPen = pRT->GetCurrentObject(OT_PEN);
			CAutoRefPtr<IRenderObj> curFont = pRT->GetCurrentObject(OT_FONT);

			CRect rcClient = GetClientRect();

			CRect DrawClient = rcClient;
			CRect Margin;
			SIZE ImgSize = m_pBgSkin->GetSkinSize();
			float ImgRateX, ImgRateY;
			ImgRateX = (float)rcClient.Width() / (float)ImgSize.cx;
			ImgRateY = (float)rcClient.Height() / (float)ImgSize.cy;
			Margin.left = ImgRateX*(float)m_MapMargin.left;
			Margin.top = ImgRateY*(float)m_MapMargin.top;
			Margin.right = ImgRateX*(float)m_MapMargin.right;
			Margin.bottom = ImgRateY*(float)m_MapMargin.bottom;

			DrawClient.DeflateRect(Margin);

			float xRate = DrawClient.Width() / (MAP_POINT_RIGHT - MAP_POINT_LEFT);
			float yRate = DrawClient.Height() / (MAP_POINT_TOP - MAP_POINT_BOTTOM);


			CAutoRefPtr<SOUI::IPen> pPen;
			pRT->CreatePen(PS_NULL, RGBA(255, 255, 255, 0), 0, &pPen);
			pRT->SelectObject(pPen);

			int Max, Min;
			Max = Min = 0;
			if (m_Data.GetCount() > 1)
				Max = Min = m_Data[0].Value;
			for (int i = 0; i < m_Data.GetCount(); i++)
			{
				Max = max(m_Data[i].Value, Max);
				Min = min(m_Data[i].Value, Min);
			}

			int MaxOriginSize = m_HeatPoMaxSize <= 1 ? min(DrawClient.Width(), DrawClient.Height())*m_HeatPoMaxSize : m_HeatPoMaxSize;
			int MinOriginSize = m_HeatPoMinSize <= 1 ? min(DrawClient.Width(), DrawClient.Height())*m_HeatPoMinSize : m_HeatPoMinSize;

			//当Max==Min时OriginRate为中间值
			float OriginRate = (MaxOriginSize - MinOriginSize) / (Max == Min ? 2.f : (float)(Max - Min));
			int OriginSize;

			for (int i = 0; i < m_Data.GetCount(); i++)
			{
				CRect Rect;
				//当Max==Min时数据点大小为中间值
				OriginSize = Max == Min ? MinOriginSize + OriginRate : MinOriginSize + (m_Data[i].Value - Min)*OriginRate;

				Rect.left = -OriginSize / 2;
				Rect.top = -OriginSize / 2;
				Rect.bottom = OriginSize / 2;
				Rect.right = OriginSize / 2;

				int x = (m_CityArray[m_Data[i].Index].Longitude - MAP_POINT_LEFT)*xRate;
				int y = 0;//纬度有误差

				//通过动态范围修正//	float  RateEx = (0.03 + (MAP_POINT_TOP - m_CityArray[m_Data[i].Index].Latitude) / (MAP_POINT_TOP - MAP_POINT_BOTTOM)*(0.15 - 0.03));
				float  RateEx = (m_MinAmend + (MAP_POINT_TOP - m_CityArray[m_Data[i].Index].Latitude) / (MAP_POINT_TOP - MAP_POINT_BOTTOM)*(m_MaxAmend - m_MinAmend));

				y = ((m_CityArray[m_Data[i].Index].Latitude - MAP_POINT_BOTTOM)*yRate) - (m_CityArray[m_Data[i].Index].Latitude - MAP_POINT_BOTTOM)*yRate*RateEx;

				Rect.OffsetRect(x + DrawClient.left, DrawClient.bottom - y);

				CAutoRefPtr<SOUI::IBrush> pBrush;
				pRT->CreateSolidColorBrush(m_Data[i].Color, &pBrush);
				pRT->SelectObject(pBrush);

				//绘制圆
				pRT->FillEllipse(Rect);

				pBrush.Release();

				if (m_ShowCity)
				{
					SIZE Size;
					pRT->MeasureText(m_CityArray[m_Data[i].Index].CityName, m_CityArray[m_Data[i].Index].CityName.GetLength(), &Size);
					pRT->TextOut(Rect.CenterPoint().x - Size.cx / 2, Rect.bottom, m_CityArray[m_Data[i].Index].CityName, m_CityArray[m_Data[i].Index].CityName.GetLength());
				}

			}

			pRT->SelectObject(curBrush);
			pRT->SelectObject(curPen);
			pRT->SelectObject(curFont);
		}
		AfterPaint(pRT, painter);
		SWindow::OnPaint(pRT);
	}


	void SChinaMap::SetMapRect(CRect &Rect)
	{
		m_MapMargin = Rect;
	}

	int SChinaMap::OnCreate(LPCREATESTRUCT lpCreateStruct)
	{
		pugi::xml_document xmlMap;
		m_CityArray.RemoveAll();
		if (SApplication::getSingleton().LoadXmlDocment(xmlMap, m_MapName, _T("DATA")))
		{
			pugi::xml_node Node = xmlMap.child(L"ChinaMap");
			SASSERT(Node);

			pugi::xml_node CityNote = Node.child(L"City");
			while (CityNote)
			{
				CityGPSData Data;
				Data.CityName = S_CW2T(CityNote.attribute(L"name").as_string());
				Data.Latitude = CityNote.attribute(L"latitude").as_float();
				Data.Longitude = CityNote.attribute(L"longitude").as_float();
				m_CityArray.Add(Data);

				CityNote = CityNote.next_sibling(L"City");
			}

		}

		return 0;
	}


	bool SChinaMap::Add(SplashesData &Data)
	{
		for (int i = 0; i < m_CityArray.GetCount(); i++)
		{
			if (m_CityArray[i].CityName.Find(Data.CityName) != -1)
			{
				Data.Index = i;
				m_Data.Add(Data);
				return true;
			}
		}
		return false;
	}

	void SChinaMap::Clear(void)
	{
		m_Data.RemoveAll();
	}

	int SChinaMap::Size(void)
	{
		return m_Data.GetCount();
	}

	SChinaMap::SplashesData* SChinaMap::GetAt(SStringW &CityName)
	{
		for (int i = 0; i < m_Data.GetCount(); i++)
		{
			if (m_Data[i].CityName == CityName)
				return &(m_Data[i]);
		}
		return NULL;
	}

	void SChinaMap::SetHeatPoRange(float Min, float Max)
	{
		m_HeatPoMinSize = Min;
		m_HeatPoMaxSize = Max;
	}

	void SChinaMap::SetLatitudeAmend(float Min, float Max)
	{
		m_MinAmend = Min;
		m_MaxAmend = Max;
	}

}