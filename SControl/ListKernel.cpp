#pragma once
#include "stdafx.h"
#include "ListKernel.h"

//template <typename T, typename M>
//SORTCTX CListKernel<T, M>::m_sctx;


template <typename T,typename M>
CListKernel<T, M>::CListKernel(CListKernel<T,M> &source)
{
	m_p = source.m_p;
	m_ListView = source.m_ListView;
	m_method = source.m_method;
	m_pOrigin = NULL;
	m_pPointer = NULL;
}
template <typename T,typename M>
CListKernel<T,M>::~CListKernel()
{

}
template <typename T,typename M>
CListKernel<T,M>::CListKernel()
{
	m_ListView = NULL;
}

template <typename T,typename M>
void CListKernel<T,M>::Init(SMCListView* ListView)
{
	m_ListView = ListView;
	if (m_ListView!=NULL)
		m_ListView->SetAdapter(this);
}

template <typename T,typename M>
bool CListKernel<T,M>::push_back(T &p)
{
	m_p.push_back(p);
	return true;
}
template <typename T,typename M>
void CListKernel<T,M>::Clear(bool IsUpdateView)
{
	m_p.clear();
	if (IsUpdateView)
		UpdateView();
}

template <typename T,typename M>
void CListKernel<T,M>::getView(int position, SWindow * pItem, pugi::xml_node xmlTemplate)
{
	if (pItem->GetChildrenCount() == 0)
	{
		pItem->InitFromXml(xmlTemplate);
	}
	std::list<T>::iterator it= m_p.begin();
	for (unsigned int index = 0; it != m_p.end(); it++, index++)
	{
		if (index == position)
		{
			pItem->SetUserData((ULONG_PTR)&(*it));//默认设置结构体指针到SWindow
			m_method.UpdateView(pItem, &(*it), index); //使用方法的UpdateView函数
			break;
		}
	}
	
}

template <typename T,typename M>
int CListKernel<T,M>::getCount()
{
	return m_p.size();
}



template <typename T,typename M>
typename std::list<T>::iterator CListKernel<T,M>::begin(void)
{
	return m_p.begin();
}
template <typename T,typename M>
typename std::list<T>::iterator CListKernel<T,M>::end(void)
{
	return m_p.end();
}

//将列表中对应的值移动到 new_list中 结构体需要实现== and =
template <typename T,typename M>
bool CListKernel<T,M>::MoveToList(CListKernel<T,M> &new_list, T &value)
{
	std::list<T>::iterator it = m_p.begin();
	for (int index = 0; it != m_p.end(); index++)
	{
		if (value == *it)
		{
			new_list.push_back(&(*it));
			it = m_p.erase(it);
			return true;
		}
		else
			it++;
	}
	return false;
}
//移动列表中选中的值到 new_list中
template <typename T, typename M>
bool  CListKernel<T, M>::MoveSelToList(CListKernel<T, M> &new_list)
{
	int sel=m_ListView->GetSel();
	if (sel < 0)
		return false;
	std::list<T>::iterator it = m_p.begin();
	for (int index = 0; it != m_p.end(); index++)
	{
		if (index == sel)
		{
			new_list.push_back(&(*it));
			it = m_p.erase(it);
			//刷新显示
			UpdateView();
			new_list.UpdateView();
			return true;
		}
		else
			it++;
	}
	return false;
}

//移除子列表中出现的值
template <typename T,typename M>
bool CListKernel<T,M>::Remove(CListKernel<T,M> &new_list)
{
	bool flag = true;
	std::list<T>::iterator it = m_p.begin();
	for (int index = 0; it != m_p.end(); index++)
	{
		std::list<T>::iterator it_sub = new_list.begin();
		for (; it_sub != new_list.end(); it_sub++)
		{
			if (*it == *it_sub)
			{
				it = m_p.erase(it);
				flag = false;
				break;
			}
		}
		//确保在删除的时候it++只执行一次
		if(flag == true)
			it++;
		else
			flag = true;

	}
	return true;
}

template <typename T,typename M>
T* CListKernel<T,M>::GetItemData(int index)
{
	std::list<T>::iterator it = m_p.begin();
	for (int n = 0; it != m_p.end(); n++,it++)
	{
		if (n == index)
		{
			return &(*it);
		}

	}
	return NULL;
}

//通过id获取数据
template <typename T, typename M>
T* CListKernel<T, M>::GetData(UINT64 id)
{
	for (T &t : m_p)
	{
		if (t.id == id)
		{
			return &t;
		}
	}
	return NULL;
}

template <typename T,typename M>
void CListKernel<T,M>::GetAllID(wchar_t *id)
{
	GetListAllID(m_p,id);
	return;
}
template <typename T,typename M>
SStringW CListKernel<T,M>::GetAllID(void)
{
	SStringW str;
	GetListAllID(m_p, str.GetBuffer(128));
	str.ReleaseBuffer();
	return str;
}


////////////////////////////////////////////////
/////////////////////////////////////////////////

template <typename T,typename M>
std::list<T>& CListKernel<T,M>::GetList(void)
{
	return m_p;
}
template <typename T, typename M>
void CListKernel<T, M>::SetList(std::list<T>& lists,bool IsUpdateView)
{
	//将原始list 保存至m_pOrigin
	m_pOrigin = &lists;
	m_p = lists;
	if (IsUpdateView)
		UpdateView();
}
template <typename T, typename M>
SMCListView* CListKernel<T, M>::GetListViewPtr(void)
{
	return m_ListView;
}
template <typename T, typename M>
int CListKernel<T, M>::GetSel(void)
{
	return m_ListView->GetSel();
}

//获取选中的ID
template <typename T, typename M>
UINT64 CListKernel<T, M>::GetSelID(void)
{
	int sel = m_ListView->GetSel();
	if (sel < 0)
		return -1;
	return GetItemData(sel)->id;
}
//获取选中的数据
template <typename T, typename M>
T* CListKernel<T, M>::GetSelUserData(void)
{
	int sel = m_ListView->GetSel();
	if (sel < 0)
		return NULL;
	return GetItemData(sel);
}
template <typename T, typename M>
bool CListKernel<T, M>::GetListAllID(const std::list<T> &lists, wchar_t *id)
{
	std::list<T>::const_iterator  it = lists.cbegin();
	for (int n = 0; it != lists.cend(); n++, it++)
	{
		//添加分割符
		if (n != 0)
			wcscat(id, L",");

		wchar_t str[64] = L"";
		_ultow((ULONG)it->id, str, 10);
		wcscat(id, str);
	}
	return true;
}
//向前移动选中项
template <typename T, typename M>
void CListKernel<T, M>::SelUpMove(void)
{
	std::list<T>::iterator  it = m_p.begin();
	int sel=m_ListView->GetSel();
	for (int i = 0; it != m_p.end(); it++, i++)
	{
		if (i == sel&&i!=0)
		{
			std::list<T>::iterator  im;
			T t = *(--it);
			im = it;
			*(it) = *(++im);
			*(++it)=t;
			////////////////////
			UpdateVisibleItems();
			m_ListView->SetSel(sel - 1);
			break;
		}
	}
	return ;
}
//向下移动选中项
template <typename T, typename M>
void CListKernel<T, M>::SelDownMove(void)
{
	std::list<T>::iterator  it = m_p.begin();
	int sel = m_ListView->GetSel();
	for (int i = 0; it != m_p.end(); it++, i++)
	{
		if (i == sel&&i != m_p.size()-1)
		{
			std::list<T>::iterator  im;
			T t = *(++it);
			im = it;
			*(it) = *(--im);
			*(--it) = t;
			////////////////////
			UpdateVisibleItems();
			m_ListView->SetSel(sel + 1);
			break;
		}
	}
	return;
}
