#include "stdafx.h"
#include "SPaging.h"

namespace SOUI
{
	SPaging::SPaging() :
		m_StrBtnSkin(L"_skin.sys.header"),//
		m_StrSelBtnSkin(L"_skin.sys.header"),
		m_PageCount(900),
		m_ShowBtnNumber(7),
		m_CurSel(0),
		m_PageJumps(true),
		m_BtnTemplate(L"pos=\"[12,0\" size=\"-1,full\" padding=\"10,0,10,0\"  display=\"0\" Cursor=\"hand\"  drawFocusRect=\"0\""),
		m_EditTemplate(L"pos=\"[10,|0\" size=\"50,full\" offset=\"0,-0.5\" inset=\"1,1,1,1\" margin=\"1,1,1,1\" number=\"1\""),
		m_TxtTemplate(L"pos=\"[10, |0\" offset=\"0,-0.5\"")
	{
		m_evtSet.addEvent(EVENTID(EventPagingChanged));

	}


	SPaging::~SPaging()
	{

	}

	int SPaging::OnCreate(LPCREATESTRUCT lpCreateStruct)
	{
		CRect rcClient = GetWindowRect();
		m_pBtnBeginPage = CreateBtn(L"首页");

		m_pBtnBeginPage->GetEventSet()->subscribeEvent(EVT_CMD, Subscriber(&SPaging::OnBtnBegin, this));
		m_pBtnLastPage = CreateBtn(L"上一页");
		m_pBtnLastPage->GetEventSet()->subscribeEvent(EVT_CMD, Subscriber(&SPaging::OnBtnLast, this));
		for (int i = 0; i < m_ShowBtnNumber; i++)
		{
			SButton *p = CreateBtn(SStringW().Format(L"%d", i + 1), i);
			p->GetEventSet()->subscribeEvent(EVT_CMD, Subscriber(&SPaging::OnBtnSel, this));
			m_pPages.Add(p);
		}
		m_pBtnNextPage = CreateBtn(L"下一页");
		m_pBtnNextPage->GetEventSet()->subscribeEvent(EVT_CMD, Subscriber(&SPaging::OnBtnNext, this));
		m_pBtnEndPage = CreateBtn(L"末页");
		m_pBtnEndPage->GetEventSet()->subscribeEvent(EVT_CMD, Subscriber(&SPaging::OnBtnEnd, this));


		if (m_PageJumps)
		{
			m_pText[1] = (SStatic*)__super::CreateChildren(SStringW().Format(L"<text %s>跳至</text>", m_TxtTemplate));
			m_pEdit = (SEdit*)__super::CreateChildren(SStringW().Format(L"<edit %s/>", m_EditTemplate));


			SStringW Test = SStringW().Format(L"<text %s>页</text>", m_TxtTemplate);
			m_pText[1] = (SStatic*)__super::CreateChildren(Test);

			m_pBtnGo = CreateBtn(L"GO");
			m_pBtnGo->GetEventSet()->subscribeEvent(EVT_CMD, Subscriber(&SPaging::OnBtnGo, this));
		}

		//test
		UpdataBtn();
		return __super::OnCreate(lpCreateStruct);
	}

	//ID 存储了索引与Text不同
	SButton* SPaging::CreateBtn(SStringW Text, int Data)
	{
		SButton *pBtn = (SButton*)__super::CreateChildren(SStringW().Format(
			L"<button  data=\"%d\" text=\"%s\" %s/>",
			Data, Text, m_BtnTemplate));
		return pBtn;
	}


	BOOL SPaging::CreateChildren(pugi::xml_node xmlNode)
	{
		if (!xmlNode) return FALSE;
		__super::CreateChildren(xmlNode);

		pugi::xml_node xmlTemplate = xmlNode.child(L"template");
		if (xmlTemplate)
		{
			pugi::xml_node xmlButton = xmlTemplate.child(L"button");
			if (xmlButton)
			{
				m_BtnTemplate = L"";
				for (pugi::xml_attribute attr = xmlButton.first_attribute(); attr; attr = attr.next_attribute())
				{
					if (wcscmp(attr.name(), L"selSkin") == 0)
					{
						m_StrSelBtnSkin = attr.value();
					}
					else
					{
						//	m_BtnTemplate += SStringW().Format(L" %s:\"%s\"", attr.name(), attr.value());
						if (wcscmp(attr.name(), L"skin") == 0)
							m_StrBtnSkin = attr.value();
						///////////////
						m_pBtnBeginPage->SetAttribute(attr.name(), attr.value());
						m_pBtnLastPage->SetAttribute(attr.name(), attr.value());
						m_pBtnNextPage->SetAttribute(attr.name(), attr.value());
						m_pBtnEndPage->SetAttribute(attr.name(), attr.value());
						m_pBtnGo->SetAttribute(attr.name(), attr.value());
						for (int i = 0; i < m_pPages.GetCount(); i++)
							m_pPages[i]->SetAttribute(attr.name(), attr.value());
					}
				}

			}
			pugi::xml_node xmlEdit = xmlTemplate.child(L"edit");
			if (xmlEdit)
			{
				m_EditTemplate = L"";
				for (pugi::xml_attribute attr = xmlEdit.first_attribute(); attr; attr = attr.next_attribute())
				{
					//	m_EditTemplate += SStringW().Format(L" %s:\"%s\"", attr.name(), attr.value());
					m_pEdit->SetAttribute(attr.name(), attr.value());
				}

			}

			pugi::xml_node xmlText = xmlTemplate.child(L"text");
			if (xmlEdit)
			{
				m_TxtTemplate = L"";
				for (pugi::xml_attribute attr = xmlText.first_attribute(); attr; attr = attr.next_attribute())
				{
					//	m_TxtTemplate += SStringW().Format(L" %s:\"%s\"", attr.name(), attr.value());
					m_pText[0]->SetAttribute(attr.name(), attr.value());
					m_pText[1]->SetAttribute(attr.name(), attr.value());
				}

			}

		}

		return TRUE;
	}

	CSize SPaging::GetDesiredSize(LPCRECT pRcContainer)
	{
		CSize Size;
		Size.cx = pRcContainer->right - pRcContainer->left;
		Size.cy = pRcContainer->bottom - pRcContainer->top;
		return Size;
	}

	void SPaging::SetCurSel(int Number)
	{
		SetSel(Number);
	}

	int SPaging::GetCurSel()
	{
		return m_CurSel;
	}

	void SPaging::SetSel(int Sel)
	{


	}

	bool SPaging::OnBtnBegin(EventArgs *pEvt)
	{
		m_CurSel = 0;
		EventPagingChanged evt(this);
		evt.nCurSel = m_CurSel;
		FireEvent(evt);

		UpdataBtn();
		return true;
	}

	bool SPaging::OnBtnLast(EventArgs *pEvt)
	{
		if (m_CurSel > 0)
			m_CurSel--;

		EventPagingChanged evt(this);
		evt.nCurSel = m_CurSel;
		FireEvent(evt);

		UpdataBtn();
		return true;
	}

	bool SPaging::OnBtnNext(EventArgs *pEvt)
	{

		if (m_CurSel < m_PageCount - 1)
			m_CurSel++;

		EventPagingChanged evt(this);
		evt.nCurSel = m_CurSel;
		FireEvent(evt);

		UpdataBtn();
		return true;
	}

	bool SPaging::OnBtnEnd(EventArgs *pEvt)
	{
		if (m_CurSel < m_PageCount)
			m_CurSel = m_PageCount - 1;

		EventPagingChanged evt(this);
		evt.nCurSel = m_CurSel;
		FireEvent(evt);

		UpdataBtn();
		return true;
	}

	bool SPaging::OnBtnSel(EventArgs *pEvt)
	{
		SWindow* pImgBtn = sobj_cast<SWindow>(pEvt->sender);
		if (pImgBtn)
		{
			m_CurSel = pImgBtn->GetUserData();



			EventPagingChanged evt(this);
			evt.nCurSel = m_CurSel;
			FireEvent(evt);

			UpdataBtn();
		}
		return true;
	}

	void SPaging::SetPageCount(int Number)
	{
		m_PageCount = Number <= 0 ? 1 : Number;
		PageNumberChange();
	}
	int SPaging::GetPageCount()
	{
		return m_PageCount;
	}

	void SPaging::PageNumberChange()
	{
		for (int i = 0; i < m_pPages.GetCount(); i++)
		{
			if (m_PageCount >= m_ShowBtnNumber)
				m_pPages[i]->SetVisible(i < m_ShowBtnNumber);
			else
				m_pPages[i]->SetVisible(i < m_PageCount);
		}
		BtnStateUpdata();
	}

	void SPaging::UpdataBtn(void)
	{
		int Neutral = m_pPages.GetCount() / 2;
		for (int i = 0; i < m_pPages.GetCount(); i++)
		{
			//m_pPages[i]->SetVisible(i<m_ShowBtnNumber);
			bool flag = m_PageCount >= m_ShowBtnNumber;
			if (flag)
				m_pPages[i]->SetVisible(i < m_ShowBtnNumber);
			else
				m_pPages[i]->SetVisible(i < m_PageCount);


			if (flag ? i < m_ShowBtnNumber : i < m_PageCount)
			{
				int Number;
				if (m_PageCount - Neutral > m_CurSel)
				{
					if (m_CurSel > Neutral)
						Number = m_CurSel - Neutral + i + 1;
					else
						Number = i + 1;
				}
				else
				{
					if (flag)
						Number = m_PageCount - m_ShowBtnNumber + i + 1;
					else
						Number = i + 1;
				}
				m_pPages[i]->SetWindowText(SStringW().Format(L"%d", Number));
				m_pPages[i]->SetUserData(Number - 1);


				m_pPages[i]->GetEventSet()->setMutedState(true);//禁止触发消息
				if (Number == m_CurSel + 1)
				{
					m_pPages[i]->SetCheck(TRUE);
					m_pPages[i]->SetAttribute(L"skin", m_StrSelBtnSkin);
				}
				else
				{
					m_pPages[i]->SetCheck(FALSE);
					m_pPages[i]->SetAttribute(L"skin", m_StrBtnSkin);
				}
				m_pPages[i]->GetEventSet()->setMutedState(false);//禁止触发消息end

			}
		}
		BtnStateUpdata();

	}

	bool SPaging::OnBtnGo(EventArgs *pEvt)
	{
		int Number = _wtoi(m_pEdit->GetWindowText()) - 1;
		if (Number >= 0 && Number < m_PageCount)
		{
			m_CurSel = Number;
			EventPagingChanged evt(this);
			evt.nCurSel = m_CurSel;
			FireEvent(evt);

			UpdataBtn();
		}
		return true;
	}

	void SPaging::TouchEvent(void)
	{
		EventPagingChanged evt(this);
		evt.nCurSel = m_CurSel;
		FireEvent(evt);
	}

	void SPaging::BtnStateUpdata()
	{
		//按钮禁用
		if (m_CurSel == m_PageCount - 1)
		{
			m_pBtnNextPage->EnableWindow(FALSE, TRUE);
			m_pBtnEndPage->EnableWindow(FALSE, TRUE);
		}
		else
		{
			m_pBtnNextPage->EnableWindow(TRUE, TRUE);
			m_pBtnEndPage->EnableWindow(TRUE, TRUE);
		}
		if (m_CurSel == 0)
		{
			m_pBtnBeginPage->EnableWindow(FALSE, TRUE);
			m_pBtnLastPage->EnableWindow(FALSE, TRUE);
		}
		else
		{
			m_pBtnBeginPage->EnableWindow(TRUE, TRUE);
			m_pBtnLastPage->EnableWindow(TRUE, TRUE);
		}

	}

}