#pragma once
/**
* Copyright (C) 2019-2050
* All rights reserved.
*
* @file       Listkernel.h
* @brief
* @version    v1.0
* @author     ph
* @date       2019-4-23
*
* Describe     列表核心模板类 适用于SOUI mclistview 控件
*/
#include "helper\SAdapterBase.h"
#include <list>
#include <iterator>


//提供一个空的方法用于调用静态函数时使用
typedef struct List_Null_t
{
	bool UpdateView(SWindow *parent, void *super, UINT index){ return true; };
	//bool 方法名(EventArgs *pEvt);
}LIST_NULL;



//M 需要实现 UpdateView(SWindow *, T*, unsigned int);  方法 
//T 数据结构 M 方法
//默认会设置T的指针到SWindow 中 (用户数据)
struct SORTCTX {
	int iCol;
	SHDSORTFLAG stFlag;
};
template <typename T, typename M>

class CListKernel:public SMcAdapterBase
{
public:
//	CListKernel(SMCListView* ListView);
	CListKernel();
	CListKernel(CListKernel<T, M> &source);
	~CListKernel();
	//初始化时调用 设置容器
	void Init(SMCListView* ListView);
	bool push_back(T &p);
	void Clear(bool IsUpdateView=false);
	typename std::list<T>::iterator begin(void);
	typename std::list<T>::iterator end(void);
	//将列表中对应的值移动到 new_list中
	bool MoveToList(CListKernel<T,M> &new_list, T &value);
	//移动列表中选中的值到 new_list中
	bool MoveSelToList(CListKernel<T, M> &new_list);

	//移除子列表中出现的值
	bool Remove(CListKernel<T,M> &new_list);

	//获取所有的ID  储存到数组中
	void GetAllID(wchar_t *id);
	SStringW GetAllID(void);

	//向前移动选中项
	void SelUpMove(void);
	//向下移动选中项
	void SelDownMove(void);
	
	//获取原始数据列表
	std::list<T>& GetList(void);
	void SetList(std::list<T>& lists,bool IsUpdateView=false);
	//获取控件指针
	SMCListView* GetListViewPtr(void);
	int GetSel(void);

	//获取选中ID 
	UINT64 GetSelID(void);
	//获取选中的用户数据 没有选中或数据为空 返回NULL 注意检查返回值
	T* GetSelUserData(void);
	CListKernel<T, M>& operator=(CListKernel<T, M>& ck)
	{ m_p = ck.m_p; m_ListView = ck.m_ListView; return this; };

	//通过index获取数据
	T* GetItemData(int index);
	//通过id获取数据
	T* GetData(UINT64 id);
	//更新显示
	void UpdateView(void){ notifyDataSetChanged(); };//m_ListView->SetAdapter(this); 
	//更新 当前在显示item
	void UpdateVisibleItems(){ m_ListView->UpdateVisibleItems(); };
	//获取原始列表指针 通过SetList设置的指针
	std::list<T> * GetOriginList(void){ return m_pOrigin; };

	static bool GetListAllID(const std::list<T> &lists, wchar_t *id);
	//返回方法指针
	M* GetMethod(void){ return &m_method; };
	//更新List中的单个信息通过id 并刷新单个显示
	void SetSingleInfo(UINT id,T &info)
	{
		std::list<T>::iterator it = m_p.begin();
		for (int index = 0; it != m_p.end(); it++, index++)
		{
			if ((*it).id == info.id)
			{
				*it = info;
				if (m_ListView != NULL)//刷新单个
					notifyItemDataChanged(index);
				break;
			}
		}
	};
	void UpdateSingleView(UINT id, T &info)
	{
		SetSingleInfo(id, info);
	};
	//设置选中
	void SetSel(int index)
	{
		m_ListView->SetSel(index);
	};
	int GetCount(void)
	{
		return m_p.size();
	}
	//保证item显示
	void  EnsureVisible(int iItem)
	{
		m_ListView->EnsureVisible(iItem);
	}
	//关于分页获取数据相关的方法
	//获取显示页面所需行数
	int GetVisbleCount(void)
	{
		int count=0;
		count = m_ListView->GetClientRect().Height() / m_ListView->GetItemLocator()->GetItemHeight(0);
		return count;
	};
	//int GetItemHeight(void)
	//{
	//	int count = 0;
	//	count = m_ListView->GetClientRect().Height() / m_ListView->GetItemLocator()->GetItemHeight(0);
	//	return count;
	//};

//	static SORTCTX m_sctx;
protected:
	virtual void getView(int position, SWindow * pItem, pugi::xml_node xmlTemplate);
	virtual SStringW GetColumnName(int iCol) const{
		return SStringW().Format(L"col%d", iCol);  //col0 ~ colXXX  UI设计时需要注意
	}	
	virtual int getCount();

	//virtual bool OnSort(int iCol, SHDSORTFLAG * stFlags, int nCols)
	//{
	//	SHDSORTFLAG stFlag = stFlags[iCol];
	//	switch (stFlag)
	//	{
	//	case ST_NULL:stFlag = ST_UP; break;
	//	case ST_DOWN:stFlag = ST_UP; break;
	//	case ST_UP:stFlag = ST_DOWN; break;
	//	}
	//	for (int i = 0; i<nCols; i++)
	//	{
	//		stFlags[i] = ST_NULL;
	//	}
	//	stFlags[iCol] = stFlag;

	//	//SORTCTX ctx = { iCol, stFlag };
	//	//设置静态变量用于传递参数
	//	m_sctx.iCol = iCol;
	//	m_sctx.stFlag = stFlag;
	//	m_p.sort(CListKernel<T, M>::my_greater);
	//	return true;
	//}
	// int my_greater(T &a, T &b) 
	//{
	//	if (m_method.sort != NULL)
	//	{
	//		m_method.sort(m_sctx.iCol, a, b);
	//	}
	//	return 0;
	//}
private:
	std::list<T> m_p;
	std::list<T> *m_pOrigin;
	M m_method;
//	SWindow *m_parent;	//父窗口指针
	SMCListView *m_ListView;	//list指针
	//类指针
	void *m_pPointer;
	
	//暂未实现
	SEdit m_edit_search; //搜索框
	SButton m_but_Search; //搜索按钮
};


